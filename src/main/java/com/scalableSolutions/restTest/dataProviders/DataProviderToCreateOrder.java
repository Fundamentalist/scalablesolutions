package com.scalableSolutions.restTest.dataProviders;

import org.testng.annotations.DataProvider;

public class DataProviderToCreateOrder {

    @DataProvider(name = "positiveTestsWithRequiredParameters")
    public static Object[][] positiveTestsWithRequiredParameters() {
        return new Object[][] {

                //  price = String и c ОДНИМ значением в дробной части

                // Применяем значение side = Buy, где первая буква заглавная.
                { "345.1", "1", "Buy" },           // quantity с минимально возможным значением. quantity = String, side = String/Buy, price = String
                { "346.1", "5555", "Buy" },        // quantity со средним значением. quantity = String, side = String/Buy, price = String
                { "347.1", "9999", "Buy" },        // quantity минимально возможным значением. quantity = String, side = String/Buy, price = String
                { "348.1", 1L, "Buy" },            // quantity с минимально возможным значением. quantity = Long, side = String/Buy, price = String
                { "349.1", 5555L, "Buy" },         // quantity со средним значением. quantity = Long, side = String/Buy, price = String
                { "350.1", 9999L, "Buy" },         // quantity минимально возможным значением. quantity = Long, side = String/Buy, price = String

                // Применяем значение side = Sell, где первая буква заглавная.
                { "351.1", "1", "Sell" },           // quantity с минимально возможным значением. quantity = String, side = String/Sell, price = String
                { "352.1", "5555", "Sell" },        // quantity со средним значением. quantity = String, side = String/Sell, price = String
                { "353.1", "9999", "Sell" },        // quantity минимально возможным значением. quantity = String, side = String/Sell, price = String
                { "354.1", 1L, "Sell" },            // quantity с минимально возможным значением. quantity = Long, side = String/Sell, price = String
                { "355.1", 5555L, "Sell" },         // quantity со средним значением. quantity = Long, side = String/Sell, price = String
                { "356.1", 9999L, "Sell" },         // quantity минимально возможным значением. quantity = Long, side = String/Sell, price = String

                //  price = String и c ДВУМЯ значением в дробной части

                // Применяем значение side = Buy, где первая буква заглавная.
                { "345.11", "1", "Buy" },           // quantity с минимально возможным значением. quantity = String, side = String/Buy, price = String
                { "346.11", "5555", "Buy" },        // quantity со средним значением. quantity = String, side = String/Buy, price = String
                { "347.11", "9999", "Buy" },        // quantity минимально возможным значением. quantity = String, side = String/Buy, price = String
                { "348.11", 1L, "Buy" },            // quantity с минимально возможным значением. quantity = Long, side = String/Buy, price = String
                { "349.11", 5555L, "Buy" },         // quantity со средним значением. quantity = Long, side = String/Buy, price = String
                { "350.11", 9999L, "Buy" },         // quantity минимально возможным значением. quantity = Long, side = String/Buy, price = String

                // Применяем значение side = Sell, где первая буква заглавная.
                { "351.11", "1", "Sell" },           // quantity с минимально возможным значением. quantity = String, side = String/Sell, price = String
                { "352.11", "5555", "Sell" },        // quantity со средним значением. quantity = String, side = String/Sell, price = String
                { "353.11", "9999", "Sell" },        // quantity минимально возможным значением. quantity = String, side = String/Sell, price = String
                { "354.11", 1L, "Sell" },            // quantity с минимально возможным значением. quantity = Long, side = String/Sell, price = String
                { "355.11", 5555L, "Sell" },         // quantity со средним значением. quantity = Long, side = String/Sell, price = String
                { "356.11", 9999L, "Sell" },         // quantity минимально возможным значением. quantity = Long, side = String/Sell, price = String


                //  price = Double и c ОДНИМ значением в дробной части

                // Применяем значение side = Buy, где первая буква заглавная.
                { 345.1, "1", "Buy" },           // quantity с минимально возможным значением. quantity = String, side = String/Buy, price = String
                { 346.1, "5555", "Buy" },        // quantity со средним значением. quantity = String, side = String/Buy, price = String
                { 347.1, "9999", "Buy" },        // quantity минимально возможным значением. quantity = String, side = String/Buy, price = String
                { 348.1, 1L, "Buy" },            // quantity с минимально возможным значением. quantity = Long, side = String/Buy, price = String
                { 349.1, 5555L, "Buy" },         // quantity со средним значением. quantity = Long, side = String/Buy, price = String
                { 350.1, 9999L, "Buy" },         // quantity минимально возможным значением. quantity = Long, side = String/Buy, price = String

                // Применяем значение side = Sell, где первая буква заглавная.
                { 351.1, "1", "Sell" },           // quantity с минимально возможным значением. quantity = String, side = String/Sell, price = String
                { 352.1, "5555", "Sell" },        // quantity со средним значением. quantity = String, side = String/Sell, price = String
                { 353.1, "9999", "Sell" },        // quantity минимально возможным значением. quantity = String, side = String/Sell, price = String
                { 354.1, 1L, "Sell" },            // quantity с минимально возможным значением. quantity = Long, side = String/Sell, price = String
                { 355.1, 5555L, "Sell" },         // quantity со средним значением. quantity = Long, side = String/Sell, price = String
                { 356.1, 9999L, "Sell" },         // quantity минимально возможным значением. quantity = Long, side = String/Sell, price = String


                //  price = Double и c ДВУМЯ значением в дробной части

                // Применяем значение side = Buy, где первая буква заглавная.
                { 345.11, "1", "Buy" },           // quantity с минимально возможным значением. quantity = String, side = String/Buy, price = String
                { 346.11, "5555", "Buy" },        // quantity со средним значением. quantity = String, side = String/Buy, price = String
                { 347.11, "9999", "Buy" },        // quantity минимально возможным значением. quantity = String, side = String/Buy, price = String
                { 348.11, 1L, "Buy" },            // quantity с минимально возможным значением. quantity = Long, side = String/Buy, price = String
                { 349.11, 5555L, "Buy" },         // quantity со средним значением. quantity = Long, side = String/Buy, price = String
                { 350.11, 9999L, "Buy" },         // quantity минимально возможным значением. quantity = Long, side = String/Buy, price = String

                // Применяем значение side = Sell, где первая буква заглавная.
                { 351.11, "1", "Sell" },           // quantity с минимально возможным значением. quantity = String, side = String/Sell, price = String
                { 352.11, "5555", "Sell" },        // quantity со средним значением. quantity = String, side = String/Sell, price = String
                { 353.11, "9999", "Sell" },        // quantity минимально возможным значением. quantity = String, side = String/Sell, price = String
                { 354.11, 1L, "Sell" },            // quantity с минимально возможным значением. quantity = Long, side = String/Sell, price = String
                { 355.11, 5555L, "Sell" },         // quantity со средним значением. quantity = Long, side = String/Sell, price = String
                { 356.11, 9999L, "Sell" },         // quantity минимально возможным значением. quantity = Long, side = String/Sell, price = String
        };
    }

    @DataProvider(name = "positiveTestsWithAllParametersWhereSideIsBuyValue")
    public static Object[][] positiveTestsWithAllParametersWhereSideIsBuyValue() {
        return new Object[][] {
                // Применяем значение side = Buy, где первая буква заглавная
                { "1", "100", "100", "Buy" },           // id с минимально возможным значением. id = String, price = String, quantity = String, side = String/Buy
                { "5555", "100", "100", "Buy" },        // id со средним значением. id = String, price = String, quantity = String, side = String/Buy
                { "9999", "100", "100", "Buy" },        // id с максимально возможным значением. id = String, price = String, quantity = String, side = String/Buy
                { 103, "100", "100", "Buy" },           // id, записанным как Integer. id = String, price = String, quantity = String, side = String/Buy
                { "104", "1", "100", "Buy" },           // price с минимально возможным ЦЕЛЫМ значением. id = String, price = String, quantity = String, side = String/Buy
                { "105", "5555", "100", "Buy" },        // price со средним ЦЕЛЫМ значением. id = String, price = String, quantity = String, side = String/Buy
                { "106", "9999", "100", "Buy" },        // price с максимально возможным ЦЕЛЫМ значением. id = String, price = String, quantity = String, side = String/Buy
                { "107", "1.1", "100", "Buy" },         // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Buy
                { "108", "5555.1", "100", "Buy" },      // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Buy
                { "109", "9999.1", "100", "Buy" },      // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Buy
                { "110", "1.01", "100", "Buy" },        // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Buy
                { "111", "5555.51", "100", "Buy" },     // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Buy
                { "112", "9999.99", "100", "Buy" },     // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Buy
                { "113", "1,1", "100", "Buy" },         // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Buy
                { "114", "5555,1", "100", "Buy" },      // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Buy
                { "115", "9999,1", "100", "Buy" },      // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Buy
                { "116", "1,01", "100", "Buy" },        // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Buy
                { "117", "5555,51", "100", "Buy" },     // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Buy
                { "118", "9999,99", "100", "Buy" },     // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Buy
                { "119", 1.1, "100", "Buy" },           // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = String, price = Double, quantity = String, side = String/Buy
                { "120", 5555.1, "100", "Buy" },        // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = String, price = Double, quantity = String, side = String/Buy
                { "121", 9999.1, "100", "Buy" },        // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = String, price = Double, quantity = String, side = String/Buy
                { "123", 1.01, "100", "Buy" },          // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = String, price = Double, quantity = String, side = String/Buy
                { "124", 5555.51, "100", "Buy" },       // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = String, price = Double, quantity = String, side = String/Buy
                { "125", 9999.99, "100", "Buy" },       // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = String, price = Double, quantity = String, side = String/Buy
                { 126, "1", "100", "Buy" },             // price с минимально возможным ЦЕЛЫМ значением при id = Integer. id = Integer, price = String, quantity = String, side = String/Buy
                { 127, "5555", "100", "Buy" },          // price со средним ЦЕЛЫМ значением при id = Integer. id = Integer, price = String, quantity = String, side = String/Buy
                { 128, "9999", "100", "Buy" },          // price с максимально возможным ЦЕЛЫМ значением при id = Integer. id = Integer, price = String, quantity = String, side = String/Buy
                { 129, "1.1", "100", "Buy" },           // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/Buy
                { 130, "5555.1", "100", "Buy" },        // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/Buy
                { 131, "9999.1", "100", "Buy" },        // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/Buy
                { 132, "1.01", "100", "Buy" },          // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String , side = String/Buy
                { 133, "5555.51", "100", "Buy" },       // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/Buy
                { 134, "9999.99", "100", "Buy" },       // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/Buy
                { 135, "1,1", "100", "Buy" },           // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Buy
                { 136, "5555,1", "100", "Buy" },        // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Buy
                { 137, "9999,1", "100", "Buy" },        // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Buy
                { 138, "1,01", "100", "Buy" },          // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Buy
                { 139, "5555,51", "100", "Buy" },       // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Buy
                { 140, "9999,99", "100", "Buy" },       // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Buy
                { 141, 1.1, "100", "Buy" },             // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = Integer, price = Double, quantity = String, side = String/Buy
                { 142, 5555.1, "100", "Buy" },          // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = Integer, price = Double, quantity = String, side = String/Buy
                { 143, 9999.1, "100", "Buy" },          // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = Integer, price = Double, quantity = String, side = String/Buy
                { 144, 1.01, "100", "Buy" },            // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = Integer, price = Double, quantity = String, side = String/Buy
                { 145, 5555.51, "100", "Buy" },         // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = Integer, price = Double, quantity = String, side = String/Buy
                { 146, 9999.99, "100", "Buy" },         // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = Integer, price = Double, quantity = String, side = String/Buy
                { "147", "1", "1", "Buy" },             // quantity с минимально возможным значением. id = String, price = String, quantity = String, side = String/Buy
                { "148", "1", "5555", "Buy" },          // quantity со средним значением. id = String, price = String, quantity = String, side = String/Buy
                { "149", "1", "9999", "Buy" },          // quantity минимально возможным значением. id = String, price = String, quantity = String, side = String/Buy
                { "150", "1", 1L, "Buy" },              // quantity с минимально возможным значением. id = String, price = String, quantity = Long, side = String/Buy
                { "151", "1", 5555L, "Buy" },           // quantity со средним значением. id = String, price = String, quantity = Long, side = String/Buy
                { "152", "1", 9999L, "Buy" },           // quantity минимально возможным значением. id = String, price = String, quantity = Long, side = String/Buy
        };
    }

    @DataProvider(name = "positiveTestsWithAllParametersWhereSideIsSellValue")
    public static Object[][] positiveTestsWithAllParametersWhereSideIsSellValue() {
        return new Object[][]{
                // Применяем значение side = Sell, где первая буква заглавная
                {"1", "100", "100", "Sell"},           // id с минимально возможным значением. id = String, price = String, quantity = String, side = String/Sell
                {"5555", "100", "100", "Sell"},        // id со средним значением. id = String, price = String, quantity = String, side = String/Sell
                {"9999", "100", "100", "Sell"},        // id с максимально возможным значением. id = String, price = String, quantity = String, side = String/Sell
                {103, "100", "100", "Sell"},           // id, записанным как Integer. id = String, price = String, quantity = String, side = String/Sell
                {"104", "1", "100", "Sell"},           // price с минимально возможным ЦЕЛЫМ значением. id = String, price = String, quantity = String, side = String/Sell
                {"105", "5555", "100", "Sell"},        // price со средним ЦЕЛЫМ значением. id = String, price = String, quantity = String, side = String/Sell
                {"106", "9999", "100", "Sell"},        // price с максимально возможным ЦЕЛЫМ значением. id = String, price = String, quantity = String, side = String/Sell
                {"107", "1.1", "100", "Sell"},         // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Sell
                {"108", "5555.1", "100", "Sell"},      // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Sell
                {"109", "9999.1", "100", "Sell"},      // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Sell
                {"110", "1.01", "100", "Sell"},        // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Sell
                {"111", "5555.51", "100", "Sell"},     // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Sell
                {"112", "9999.99", "100", "Sell"},     // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Sell
                {"113", "1,1", "100", "Sell"},         // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Sell
                {"114", "5555,1", "100", "Sell"},      // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Sell
                {"115", "9999,1", "100", "Sell"},      // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Sell
                {"116", "1,01", "100", "Sell"},        // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Sell
                {"117", "5555,51", "100", "Sell"},     // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Sell
                {"118", "9999,99", "100", "Sell"},     // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Sell
                {"119", 1.1, "100", "Sell"},           // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = String, price = Double, quantity = String, side = String/Sell
                {"120", 5555.1, "100", "Sell"},        // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = String, price = Double, quantity = String, side = String/Sell
                {"121", 9999.1, "100", "Sell"},        // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = String, price = Double, quantity = String, side = String/Sell
                {"123", 1.01, "100", "Sell"},          // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = String, price = Double, quantity = String, side = String/Sell
                {"124", 5555.51, "100", "Sell"},       // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = String, price = Double, quantity = String, side = String/Sell
                {"125", 9999.99, "100", "Sell"},       // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = String, price = Double, quantity = String, side = String/Sell
                {126, "1", "100", "Sell"},             // price с минимально возможным ЦЕЛЫМ значением при id = Integer. id = Integer, price = String, quantity = String, side = String/Sell
                {127, "5555", "100", "Sell"},          // price со средним ЦЕЛЫМ значением при id = Integer. id = Integer, price = String, quantity = String, side = String/Sell
                {128, "9999", "100", "Sell"},          // price с максимально возможным ЦЕЛЫМ значением при id = Integer. id = Integer, price = String, quantity = String, side = String/Sell
                {129, "1.1", "100", "Sell"},           // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/Sell
                {130, "5555.1", "100", "Sell"},        // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/Sell
                {131, "9999.1", "100", "Sell"},        // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/Sell
                {132, "1.01", "100", "Sell"},          // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String , side = String/Sell
                {133, "5555.51", "100", "Sell"},       // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/Sell
                {134, "9999.99", "100", "Sell"},       // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/Sell
                {135, "1,1", "100", "Sell"},           // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Sell
                {136, "5555,1", "100", "Sell"},        // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Sell
                {137, "9999,1", "100", "Sell"},        // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Sell
                {138, "1,01", "100", "Sell"},          // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Sell
                {139, "5555,51", "100", "Sell"},       // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Sell
                {140, "9999,99", "100", "Sell"},       // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Sell
                {141, 1.1, "100", "Sell"},             // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = Integer, price = Double, quantity = String, side = String/Sell
                {142, 5555.1, "100", "Sell"},          // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = Integer, price = Double, quantity = String, side = String/Sell
                {143, 9999.1, "100", "Sell"},          // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = Integer, price = Double, quantity = String, side = String/Sell
                {144, 1.01, "100", "Sell"},            // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = Integer, price = Double, quantity = String, side = String/Sell
                {145, 5555.51, "100", "Sell"},         // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = Integer, price = Double, quantity = String, side = String/Sell
                {146, 9999.99, "100", "Sell"},         // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = Integer, price = Double, quantity = String, side = String/Sell
                {"147", "1", "1", "Sell"},             // quantity с минимально возможным значением. id = String, price = String, quantity = String, side = String/Sell
                {"148", "1", "5555", "Sell"},          // quantity со средним значением. id = String, price = String, quantity = String, side = String/Sell
                {"149", "1", "9999", "Sell"},          // quantity минимально возможным значением. id = String, price = String, quantity = String, side = String/Sell
                {"150", "1", 1L, "Sell"},              // quantity с минимально возможным значением. id = String, price = String, quantity = Long, side = String/Sell
                {"151", "1", 5555L, "Sell"},           // quantity со средним значением. id = String, price = String, quantity = Long, side = String/Sell
                {"152", "1", 9999L, "Sell"},           // quantity минимально возможным значением. id = String, price = String, quantity = Long, side = String/Sell
        };
    }


    @DataProvider(name = "negativeTestsWithOutPriceParameter")
    public static Object[][] negativeTestsWithOutPriceParameter() {
        return new Object[][]{

                // Применяем значение side = Buy, где первая буква заглавная
                {"1", "Buy"},           // quantity с минимально возможным значением. quantity = String, side = String/Buy
                {"5555", "Buy"},        // quantity со средним значением. quantity = String, side = String/Buy
                {"9999", "Buy"},        // quantity минимально возможным значением. quantity = String, side = String/Buy
                {1L, "Buy"},            // quantity с минимально возможным значением. quantity = Long, side = String/Buy
                {5555L, "Buy"},         // quantity со средним значением. quantity = Long, side = String/Buy
                {9999L, "Buy"},         // quantity минимально возможным значением. quantity = Long, side = String/Buy

                // Применяем значение side = Sell, где первая буква заглавная
                {"1", "Sell"},           // quantity с минимально возможным значением. quantity = String, side = String/Sell
                {"5555", "Sell"},        // quantity со средним значением. quantity = String, side = String/Sell
                {"9999", "Sell"},        // quantity минимально возможным значением. quantity = String, side = String/Sell
                {1L, "Sell"},            // quantity с минимально возможным значением. quantity = Long, side = String/Sell
                {5555L, "Sell"},         // quantity со средним значением. quantity = Long, side = String/Sell
                {9999L, "Sell"},         // quantity минимально возможным значением. quantity = Long, side = String/Sell
        };
    }

    @DataProvider(name = "negativeTestsWithOutPriceParameterButHaveID")
    public static Object[][] negativeTestsWithOutPriceParameterButHaveID() {
        return new Object[][] {

                // Применяем значение side = Buy, где первая буква заглавная. id = String
                { "345", "1", "Buy" },           // quantity с минимально возможным значением. quantity = String, side = String/Buy, id = String
                { "346", "5555", "Buy" },        // quantity со средним значением. quantity = String, side = String/Buy, id = String
                { "347", "9999", "Buy" },        // quantity минимально возможным значением. quantity = String, side = String/Buy, id = String
                { "348", 1L, "Buy" },            // quantity с минимально возможным значением. quantity = Long, side = String/Buy, id = String
                { "349", 5555L, "Buy" },         // quantity со средним значением. quantity = Long, side = String/Buy, id = String
                { "350", 9999L, "Buy" },         // quantity минимально возможным значением. quantity = Long, side = String/Buy, id = String

                // Применяем значение side = Sell, где первая буква заглавная. id = String
                { "351", "1", "Sell" },           // quantity с минимально возможным значением. quantity = String, side = String/Sell, id = String
                { "352", "5555", "Sell" },        // quantity со средним значением. quantity = String, side = String/Sell, id = String
                { "353", "9999", "Sell" },        // quantity минимально возможным значением. quantity = String, side = String/Sell, id = String
                { "354", 1L, "Sell" },            // quantity с минимально возможным значением. quantity = Long, side = String/Sell, id = String
                { "355", 5555L, "Sell" },         // quantity со средним значением. quantity = Long, side = String/Sell, id = String
                { "356", 9999L, "Sell" },         // quantity минимально возможным значением. quantity = Long, side = String/Sell, id = String

                // Применяем значение side = Buy, где первая буква заглавная. id = Integer
                { 383, "1", "Buy" },           // quantity с минимально возможным значением. quantity = String, side = String/Buy, id = Integer
                { 384, "5555", "Buy" },        // quantity со средним значением. quantity = String, side = String/Buy, id = Integer
                { 385, "9999", "Buy" },        // quantity минимально возможным значением. quantity = String, side = String/Buy, id = Integer
                { 386, 1L, "Buy" },            // quantity с минимально возможным значением. quantity = Long, side = String/Buy, id = Integer
                { 387, 5555L, "Buy" },         // quantity со средним значением. quantity = Long, side = String/Buy, id = Integer
                { 388, 9999L, "Buy" },         // quantity минимально возможным значением. quantity = Long, side = String/Buy, id = Integer

                // Применяем значение side = Sell, где первая буква заглавная. id = Integer
                { 389, "1", "Sell" },           // quantity с минимально возможным значением. quantity = String, side = String/Sell, id = Integer
                { 390, "5555", "Sell" },        // quantity со средним значением. quantity = String, side = String/Sell, id = Integer
                { 391, "9999", "Sell" },        // quantity минимально возможным значением. quantity = String, side = String/Sell, id = Integer
                { 392, 1L, "Sell" },            // quantity с минимально возможным значением. quantity = Long, side = String/Sell, id = Integer
                { 393, 5555L, "Sell" },         // quantity со средним значением. quantity = Long, side = String/Sell, id = Integer
                { 394, 9999L, "Sell" },         // quantity минимально возможным значением. quantity = Long, side = String/Sell, id = Integer
        };
    }

    @DataProvider(name = "negativeTestsSettingIncorrectValuesIdIsNotIntegerOrString")
    public static Object[][] negativeTestsSettingIncorrectValuesIdIsNotIntegerOrString() {
        return new Object[][]{
                // Значения для тестов, где id имеет другие типы значений отличные от допустимых Integer/String
                {0.1, "100", "100", "Buy"},                                  // id имеет тип double с одним знаком в дробной части
                {0.11, "100", "100", "Buy"},                                 // id имеет тип double с двумя знаками в дробной части
                {8.5F, "100", "100", "Buy"},                                 // id имеет тип float с одним знаком в дробной части
                {2147483649L, "100", "100", "Buy"},                          // id имеет тип long
                {0x6F, "100", "100", "Buy"},                                 // 16-тиричная система, число 111
                {true, "100", "100", "Buy"},                                 // id имеет тип boolean = true
                {false, "100", "100", "Buy"},                                // id имеет тип boolean = false
        };
    }

    @DataProvider(name = "negativeTestsWhereSideIsbuyValue")
    public static Object[][] negativeTestsWhereSideIsbuyValue() {
        return new Object[][]{
                // Применяем значение side = buy, где все буквы НЕ заглавные
                { "1", "100", "100", "buy" },        // id с минимально возможным значением. id = String, price = String, quantity = String, side = String/buy
                { "5555", "100", "100", "buy" },     // id со средним значением. id = String, price = String, quantity = String, side = String/buy
                { "9999", "100", "100", "buy" },     // id с максимально возможным значением. id = String, price = String, quantity = String, side = String/buy
                { 2, "100", "100", "buy" },          // id, записанным как Integer. id = String, price = String, quantity = String, side = String/buy
                { "3", "1", "100", "buy" },          // price с минимально возможным ЦЕЛЫМ значением. id = String, price = String, quantity = String, side = String/buy
                { "4", "5555", "100", "buy" },       // price со средним ЦЕЛЫМ значением. id = String, price = String, quantity = String, side = String/buy
                { "5", "9999", "100", "buy" },       // price с максимально возможным ЦЕЛЫМ значением. id = String, price = String, quantity = String, side = String/buy
                { "6", "1.1", "100", "buy" },        // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/buy
                { "7", "5555.1", "100", "buy" },     // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/buy
                { "8", "9999.1", "100", "buy" },     // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/buy
                { "9", "1.01", "100", "buy" },       // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/buy
                { "10", "5555.51", "100", "buy" },   // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/buy
                { "11", "9999.99", "100", "buy" },   // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/buy
                { "12", "1,1", "100", "buy" },       // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/buy
                { "13", "5555,1", "100", "buy" },    // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/buy
                { "14", "9999,1", "100", "buy" },    // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/buy
                { "15", "1,01", "100", "buy" },      // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/buy
                { "16", "5555,51", "100", "buy" },   // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/buy
                { "17", "9999,99", "100", "buy" },   // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/buy
                { "18", 1.1, "100", "buy" },         // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = String, price = Double, quantity = String, side = String/buy
                { "19", 5555.1, "100", "buy" },      // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = String, price = Double, quantity = String, side = String/buy
                { "20", 9999.1, "100", "buy" },      // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = String, price = Double, quantity = String, side = String/buy
                { "21", 1.01, "100", "buy" },        // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = String, price = Double, quantity = String, side = String/buy
                { "22", 5555.51, "100", "buy" },     // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = String, price = Double, quantity = String, side = String/buy
                { "23", 9999.99, "100", "buy" },     // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = String, price = Double, quantity = String, side = String/buy
                { 24, "1", "100", "buy" },           // price с минимально возможным ЦЕЛЫМ значением при id = Integer. id = Integer, price = String, quantity = String, side = String/buy
                { 25, "5555", "100", "buy" },        // price со средним ЦЕЛЫМ значением при id = Integer. id = Integer, price = String, quantity = String, side = String/buy
                { 26, "9999", "100", "buy" },        // price с максимально возможным ЦЕЛЫМ значением при id = Integer. id = Integer, price = String, quantity = String, side = String/buy
                { 27, "1.1", "100", "buy" },         // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/buy
                { 28, "5555.1", "100", "buy" },      // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/buy
                { 29, "9999.1", "100", "buy" },      // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/buy
                { 30, "1.01", "100", "buy" },        // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String , side = String/buy
                { 31, "5555.51", "100", "buy" },     // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/buy
                { 32, "9999.99", "100", "buy" },     // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/buy
                { 33, "1,1", "100", "buy" },         // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/buy
                { 34, "5555,1", "100", "buy" },      // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/buy
                { 35, "9999,1", "100", "buy" },      // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/buy
                { 36, "1,01", "100", "buy" },        // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/buy
                { 37, "5555,51", "100", "buy" },     // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/buy
                { 38, "9999,99", "100", "buy" },     // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/buy
                { 39, 1.1, "100", "buy" },           // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = Integer, price = Double, quantity = String, side = String/buy
                { 40, 5555.1, "100", "buy" },        // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = Integer, price = Double, quantity = String, side = String/buy
                { 41, 9999.1, "100", "buy" },        // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = Integer, price = Double, quantity = String, side = String/buy
                { 42, 1.01, "100", "buy" },          // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = Integer, price = Double, quantity = String, side = String/buy
                { 43, 5555.51, "100", "buy" },       // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = Integer, price = Double, quantity = String, side = String/buy
                { 44, 9999.99, "100", "buy" },       // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = Integer, price = Double, quantity = String, side = String/buy
                { "45", "1", "1", "buy" },           // quantity с минимально возможным значением. id = String, price = String, quantity = String, side = String/buy
                { "46", "1", "5555", "buy" },        // quantity со средним значением. id = String, price = String, quantity = String, side = String/buy
                { "47", "1", "9999", "buy" },        // quantity минимально возможным значением. id = String, price = String, quantity = String, side = String/buy
                { "48", "1", 1L, "buy" },            // quantity с минимально возможным значением. id = String, price = String, quantity = Long, side = String/buy
                { "49", "1", 5555L, "buy" },         // quantity со средним значением. id = String, price = String, quantity = Long, side = String/buy
                { "50", "1", 9999L, "buy" },         // quantity минимально возможным значением. id = String, price = String, quantity = Long, side = String/buy
        };
    }

    @DataProvider(name = "negativeTestsWhereSideIssellValue")
    public static Object[][] negativeTestsWhereSideIssellValue() {
        return new Object[][]{
                // Применяем значение side = sell, где все буквы НЕ заглавные
                { "1", "100", "100", "sell" },        // id с минимально возможным значением. id = String, price = String, quantity = String, side = String/sell
                { "5555", "100", "100", "sell" },     // id со средним значением. id = String, price = String, quantity = String, side = String/sell
                { "9999", "100", "100", "sell" },     // id с максимально возможным значением. id = String, price = String, quantity = String, side = String/sell
                { 2, "100", "100", "sell" },          // id, записанным как Integer. id = String, price = String, quantity = String, side = String/sell
                { "3", "1", "100", "sell" },          // price с минимально возможным ЦЕЛЫМ значением. id = String, price = String, quantity = String, side = String/sell
                { "4", "5555", "100", "sell" },       // price со средним ЦЕЛЫМ значением. id = String, price = String, quantity = String, side = String/sell
                { "5", "9999", "100", "sell" },       // price с максимально возможным ЦЕЛЫМ значением. id = String, price = String, quantity = String, side = String/sell
                { "6", "1.1", "100", "sell" },        // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/sell
                { "7", "5555.1", "100", "sell" },     // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/sell
                { "8", "9999.1", "100", "sell" },     // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/sell
                { "9", "1.01", "100", "sell" },       // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/sell
                { "10", "5555.51", "100", "sell" },   // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/sell
                { "11", "9999.99", "100", "sell" },   // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/sell
                { "12", "1,1", "100", "sell" },       // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/sell
                { "13", "5555,1", "100", "sell" },    // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/sell
                { "14", "9999,1", "100", "sell" },    // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/sell
                { "15", "1,01", "100", "sell" },      // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/sell
                { "16", "5555,51", "100", "sell" },   // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/sell
                { "17", "9999,99", "100", "sell" },   // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/sell
                { "18", 1.1, "100", "sell" },         // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = String, price = Double, quantity = String, side = String/sell
                { "19", 5555.1, "100", "sell" },      // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = String, price = Double, quantity = String, side = String/sell
                { "20", 9999.1, "100", "sell" },      // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = String, price = Double, quantity = String, side = String/sell
                { "21", 1.01, "100", "sell" },        // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = String, price = Double, quantity = String, side = String/sell
                { "22", 5555.51, "100", "sell" },     // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = String, price = Double, quantity = String, side = String/sell
                { "23", 9999.99, "100", "sell" },     // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = String, price = Double, quantity = String, side = String/sell
                { 24, "1", "100", "sell" },           // price с минимально возможным ЦЕЛЫМ значением при id = Integer. id = Integer, price = String, quantity = String, side = String/sell
                { 25, "5555", "100", "sell" },        // price со средним ЦЕЛЫМ значением при id = Integer. id = Integer, price = String, quantity = String, side = String/sell
                { 26, "9999", "100", "sell" },        // price с максимально возможным ЦЕЛЫМ значением при id = Integer. id = Integer, price = String, quantity = String, side = String/sell
                { 27, "1.1", "100", "sell" },         // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/sell
                { 28, "5555.1", "100", "sell" },      // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/sell
                { 29, "9999.1", "100", "sell" },      // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/sell
                { 30, "1.01", "100", "sell" },        // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String , side = String/sell
                { 31, "5555.51", "100", "sell" },     // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/sell
                { 32, "9999.99", "100", "sell" },     // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/sell
                { 33, "1,1", "100", "sell" },         // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/sell
                { 34, "5555,1", "100", "sell" },      // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/sell
                { 35, "9999,1", "100", "sell" },      // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/sell
                { 36, "1,01", "100", "sell" },        // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/sell
                { 37, "5555,51", "100", "sell" },     // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/sell
                { 38, "9999,99", "100", "sell" },     // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/sell
                { 39, 1.1, "100", "sell" },           // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = Integer, price = Double, quantity = String, side = String/sell
                { 40, 5555.1, "100", "sell" },        // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = Integer, price = Double, quantity = String, side = String/sell
                { 41, 9999.1, "100", "sell" },        // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = Integer, price = Double, quantity = String, side = String/sell
                { 42, 1.01, "100", "sell" },          // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = Integer, price = Double, quantity = String, side = String/sell
                { 43, 5555.51, "100", "sell" },       // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = Integer, price = Double, quantity = String, side = String/sell
                { 44, 9999.99, "100", "sell" },       // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = Integer, price = Double, quantity = String, side = String/sell
                { "45", "1", "1", "sell" },           // quantity с минимально возможным значением. id = String, price = String, quantity = String, side = String/sell
                { "46", "1", "5555", "sell" },        // quantity со средним значением. id = String, price = String, quantity = String, side = String/sell
                { "47", "1", "9999", "sell" },        // quantity минимально возможным значением. id = String, price = String, quantity = String, side = String/sell
                { "48", "1", 1L, "sell" },            // quantity с минимально возможным значением. id = String, price = String, quantity = Long, side = String/sell
                { "49", "1", 5555L, "sell" },         // quantity со средним значением. id = String, price = String, quantity = Long, side = String/sell
                { "50", "1", 9999L, "sell" },         // quantity минимально возможным значением. id = String, price = String, quantity = Long, side = String/sell
        };
    }

    @DataProvider(name = "negativeTestsWhereSideIsBUYValue")
    public static Object[][] negativeTestsWhereSideIsBUYValue() {
        return new Object[][]{
                // Применяем значение side = BUY, где все буквы заглавные
                {"1", "100", "100", "BUY"},           // id с минимально возможным значением. id = String, price = String, quantity = String, side = String/Buy
                {"5555", "100", "100", "BUY"},        // id со средним значением. id = String, price = String, quantity = String, side = String/Buy
                {"9999", "100", "100", "BUY"},        // id с максимально возможным значением. id = String, price = String, quantity = String, side = String/Buy
                {103, "100", "100", "BUY"},           // id, записанным как Integer. id = String, price = String, quantity = String, side = String/Buy
                {"104", "1", "100", "BUY"},           // price с минимально возможным ЦЕЛЫМ значением. id = String, price = String, quantity = String, side = String/Buy
                {"105", "5555", "100", "BUY"},        // price со средним ЦЕЛЫМ значением. id = String, price = String, quantity = String, side = String/Buy
                {"106", "9999", "100", "BUY"},        // price с максимально возможным ЦЕЛЫМ значением. id = String, price = String, quantity = String, side = String/Buy
                {"107", "1.1", "100", "BUY"},         // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Buy
                {"108", "5555.1", "100", "BUY"},      // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Buy
                {"109", "9999.1", "100", "BUY"},      // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Buy
                {"110", "1.01", "100", "BUY"},        // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Buy
                {"111", "5555.51", "100", "BUY"},     // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Buy
                {"112", "9999.99", "100", "BUY"},     // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Buy
                {"113", "1,1", "100", "BUY"},         // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Buy
                {"114", "5555,1", "100", "BUY"},      // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Buy
                {"115", "9999,1", "100", "BUY"},      // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Buy
                {"116", "1,01", "100", "BUY"},        // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Buy
                {"117", "5555,51", "100", "BUY"},     // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Buy
                {"118", "9999,99", "100", "BUY"},     // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Buy
                {"119", 1.1, "100", "BUY"},           // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = String, price = Double, quantity = String, side = String/Buy
                {"120", 5555.1, "100", "BUY"},        // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = String, price = Double, quantity = String, side = String/Buy
                {"121", 9999.1, "100", "BUY"},        // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = String, price = Double, quantity = String, side = String/Buy
                {"123", 1.01, "100", "BUY"},          // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = String, price = Double, quantity = String, side = String/Buy
                {"124", 5555.51, "100", "BUY"},       // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = String, price = Double, quantity = String, side = String/Buy
                {"125", 9999.99, "100", "BUY"},       // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = String, price = Double, quantity = String, side = String/Buy
                {126, "1", "100", "BUY"},             // price с минимально возможным ЦЕЛЫМ значением при id = Integer. id = Integer, price = String, quantity = String, side = String/Buy
                {127, "5555", "100", "BUY"},          // price со средним ЦЕЛЫМ значением при id = Integer. id = Integer, price = String, quantity = String, side = String/Buy
                {128, "9999", "100", "BUY"},          // price с максимально возможным ЦЕЛЫМ значением при id = Integer. id = Integer, price = String, quantity = String, side = String/Buy
                {129, "1.1", "100", "BUY"},           // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/Buy
                {130, "5555.1", "100", "BUY"},        // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/Buy
                {131, "9999.1", "100", "BUY"},        // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/Buy
                {132, "1.01", "100", "BUY"},          // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String , side = String/Buy
                {133, "5555.51", "100", "BUY"},       // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/Buy
                {134, "9999.99", "100", "BUY"},       // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/Buy
                {135, "1,1", "100", "BUY"},           // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Buy
                {136, "5555,1", "100", "BUY"},        // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Buy
                {137, "9999,1", "100", "BUY"},        // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Buy
                {138, "1,01", "100", "BUY"},          // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Buy
                {139, "5555,51", "100", "BUY"},       // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Buy
                {140, "9999,99", "100", "BUY"},       // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Buy
                {141, 1.1, "100", "BUY"},             // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = Integer, price = Double, quantity = String, side = String/Buy
                {142, 5555.1, "100", "BUY"},          // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = Integer, price = Double, quantity = String, side = String/Buy
                {143, 9999.1, "100", "BUY"},          // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = Integer, price = Double, quantity = String, side = String/Buy
                {144, 1.01, "100", "BUY"},            // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = Integer, price = Double, quantity = String, side = String/Buy
                {145, 5555.51, "100", "BUY"},         // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = Integer, price = Double, quantity = String, side = String/Buy
                {146, 9999.99, "100", "BUY"},         // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = Integer, price = Double, quantity = String, side = String/Buy
                {"147", "1", "1", "BUY"},             // quantity с минимально возможным значением. id = String, price = String, quantity = String, side = String/Buy
                {"148", "1", "5555", "BUY"},          // quantity со средним значением. id = String, price = String, quantity = String, side = String/Buy
                {"149", "1", "9999", "BUY"},          // quantity минимально возможным значением. id = String, price = String, quantity = String, side = String/Buy
                {"150", "1", 1L, "BUY"},              // quantity с минимально возможным значением. id = String, price = String, quantity = Long, side = String/Buy
                {"151", "1", 5555L, "BUY"},           // quantity со средним значением. id = String, price = String, quantity = Long, side = String/Buy
                {"152", "1", 9999L, "BUY"},           // quantity минимально возможным значением. id = String, price = String, quantity = Long, side = String/Buy
        };
    }

    @DataProvider(name = "positiveTestsWhereSideIsSELLValue")
    public static Object[][] negativeTestsWhereSideIsSELLValue() {
        return new Object[][]{
                // Применяем значение side = SELL, где все буквы заглавные
                {"1", "100", "100", "SELL"},           // id с минимально возможным значением. id = String, price = String, quantity = String, side = String/Sell
                {"5555", "100", "100", "SELL"},        // id со средним значением. id = String, price = String, quantity = String, side = String/Sell
                {"9999", "100", "100", "SELL"},        // id с максимально возможным значением. id = String, price = String, quantity = String, side = String/Sell
                {103, "100", "100", "SELL"},           // id, записанным как Integer. id = String, price = String, quantity = String, side = String/Sell
                {"104", "1", "100", "SELL"},           // price с минимально возможным ЦЕЛЫМ значением. id = String, price = String, quantity = String, side = String/Sell
                {"105", "5555", "100", "SELL"},        // price со средним ЦЕЛЫМ значением. id = String, price = String, quantity = String, side = String/Sell
                {"106", "9999", "100", "SELL"},        // price с максимально возможным ЦЕЛЫМ значением. id = String, price = String, quantity = String, side = String/Sell
                {"107", "1.1", "100", "SELL"},         // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Sell
                {"108", "5555.1", "100", "SELL"},      // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Sell
                {"109", "9999.1", "100", "SELL"},      // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Sell
                {"110", "1.01", "100", "SELL"},        // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Sell
                {"111", "5555.51", "100", "SELL"},     // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Sell
                {"112", "9999.99", "100", "SELL"},     // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = String, price = String, quantity = String, side = String/Sell
                {"113", "1,1", "100", "SELL"},         // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Sell
                {"114", "5555,1", "100", "SELL"},      // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Sell
                {"115", "9999,1", "100", "SELL"},      // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Sell
                {"116", "1,01", "100", "SELL"},        // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Sell
                {"117", "5555,51", "100", "SELL"},     // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Sell
                {"118", "9999,99", "100", "SELL"},     // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = String, price = String, quantity = String, side = String/Sell
                {"119", 1.1, "100", "SELL"},           // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = String, price = Double, quantity = String, side = String/Sell
                {"120", 5555.1, "100", "SELL"},        // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = String, price = Double, quantity = String, side = String/Sell
                {"121", 9999.1, "100", "SELL"},        // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = String, price = Double, quantity = String, side = String/Sell
                {"123", 1.01, "100", "SELL"},          // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = String, price = Double, quantity = String, side = String/Sell
                {"124", 5555.51, "100", "SELL"},       // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = String, price = Double, quantity = String, side = String/Sell
                {"125", 9999.99, "100", "SELL"},       // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = String, price = Double, quantity = String, side = String/Sell
                {126, "1", "100", "SELL"},             // price с минимально возможным ЦЕЛЫМ значением при id = Integer. id = Integer, price = String, quantity = String, side = String/Sell
                {127, "5555", "100", "SELL"},          // price со средним ЦЕЛЫМ значением при id = Integer. id = Integer, price = String, quantity = String, side = String/Sell
                {128, "9999", "100", "SELL"},          // price с максимально возможным ЦЕЛЫМ значением при id = Integer. id = Integer, price = String, quantity = String, side = String/Sell
                {129, "1.1", "100", "SELL"},           // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/Sell
                {130, "5555.1", "100", "SELL"},        // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/Sell
                {131, "9999.1", "100", "SELL"},        // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/Sell
                {132, "1.01", "100", "SELL"},          // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String , side = String/Sell
                {133, "5555.51", "100", "SELL"},       // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/Sell
                {134, "9999.99", "100", "SELL"},       // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ".". id = Integer, price = String, quantity = String, side = String/Sell
                {135, "1,1", "100", "SELL"},           // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Sell
                {136, "5555,1", "100", "SELL"},        // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Sell
                {137, "9999,1", "100", "SELL"},        // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Sell
                {138, "1,01", "100", "SELL"},          // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Sell
                {139, "5555,51", "100", "SELL"},       // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Sell
                {140, "9999,99", "100", "SELL"},       // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. Для разделение целой и дробной частей используем знак - ",". id = Integer, price = String, quantity = String, side = String/Sell
                {141, 1.1, "100", "SELL"},             // price с минимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = Integer, price = Double, quantity = String, side = String/Sell
                {142, 5555.1, "100", "SELL"},          // price со средним НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = Integer, price = Double, quantity = String, side = String/Sell
                {143, 9999.1, "100", "SELL"},          // price с максимально возможным НЕЦЕЛЫМ значением, с ОДНИМ значением в дробной части. id = Integer, price = Double, quantity = String, side = String/Sell
                {144, 1.01, "100", "SELL"},            // price с минимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = Integer, price = Double, quantity = String, side = String/Sell
                {145, 5555.51, "100", "SELL"},         // price со средним НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = Integer, price = Double, quantity = String, side = String/Sell
                {146, 9999.99, "100", "SELL"},         // price с максимально возможным НЕЦЕЛЫМ значением, с ДВУМЯ значениями в дробной части. id = Integer, price = Double, quantity = String, side = String/Sell
                {"147", "1", "1", "SELL"},             // quantity с минимально возможным значением. id = String, price = String, quantity = String, side = String/Sell
                {"148", "1", "5555", "SELL"},          // quantity со средним значением. id = String, price = String, quantity = String, side = String/Sell
                {"149", "1", "9999", "SELL"},          // quantity минимально возможным значением. id = String, price = String, quantity = String, side = String/Sell
                {"150", "1", 1L, "SELL"},              // quantity с минимально возможным значением. id = String, price = String, quantity = Long, side = String/Sell
                {"151", "1", 5555L, "SELL"},           // quantity со средним значением. id = String, price = String, quantity = Long, side = String/Sell
                {"152", "1", 9999L, "SELL"},           // quantity минимально возможным значением. id = String, price = String, quantity = Long, side = String/Sell
        };
    }
}
