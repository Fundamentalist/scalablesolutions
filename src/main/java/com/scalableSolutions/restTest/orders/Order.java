package com.scalableSolutions.restTest.orders;

import com.alibaba.fastjson.JSONObject;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class Order {

    /**
     * Метод получения ордера
     * @orderId - Идентификатор для ордера. Ордер - это заявка на бирже на покупку или продажу некоторого количества
     * товара (валюты, акции, облигации или другого товара)
     * @return - информация о ордере
     */
    public static String getOrder(String orderId){
        Response response = given().headers("Content-Type", "application/json")
                                   .get("api/order?id=" + orderId);
        return response.getBody().asString();
    }

    /**
     * Метод создания ордера
     * @orderId - Идентификатор для ордера. Ордер - это заявка на бирже на покупку или продажу некоторого количества
     * товара (валюты, акции, облигации или другого товара)
     * @price - цена, по которой пользователь готов покупать или продавать товар.
     * @quantity - количество товара для продажи или покупки
     * @side - тип заявки
     */
    public static void createOrder(String orderId, String price, String quantity, String side){
        JSONObject body = new JSONObject();
        body.put("id", orderId);
        body.put("price", price);
        body.put("quantity", quantity);
        body.put("side", side);

        given().header("Content-type", "application/json")
               .body(body.toString())
        .when().post("/api/order/create")
        .then().extract()
               .response();
    }

    /**
     * Метод удаления ордера по id
     * @orderId - Идентификатор для ордера. Ордер - это заявка на бирже на покупку или продажу некоторого количества
     * товара (валюты, акции, облигации или другого товара)
     */
    public static void removeOrder(String orderId){
        given().delete("api/order?id=" + orderId);
    }

    /**
     * Метод для очистки всех ордеров
     */
    public static void cleanOrders(){
        given().headers("Content-Type", "application/json")
                .get("api/order/clean")
                .then()
                .statusCode(200);
    }

}
