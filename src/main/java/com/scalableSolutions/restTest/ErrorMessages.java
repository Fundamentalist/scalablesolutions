package com.scalableSolutions.restTest;

public class ErrorMessages {

    public static String HEADER_IS_INCORRECT = "Header is incorrect";
    public static String ORDER_NOT_FOUND = "Order not found";
    public static String YOU_NEED_TO_PASS_PRICE_QUANTITY_AND_SIDE_PARAMS = "You need to pass 'price', 'quantity' and 'side' params";
    public static String PARAM_TEST_IS_NOT_EXIST = "Param 'test' is not exist";
    public static String YOU_NEED_TO_PASS_PRICE_PARAM = "You need to pass 'price' param";
    public static String SIDE_INCORRECT_VALUE = "side: IncorrectValue";
    public static String ID_SHOULD_BE_AN_INTEGER = "ID should be an Integer";
    public static String ID_CANT_BE_LESS_OR_EQUAL_THAN_0 = "ID can't be less or equal than 0";
    public static String ID_CANT_BE_MORE_OR_EQUAL_THAN_10000 = "ID can't be more or equal than 10000";
}
