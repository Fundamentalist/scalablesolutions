package com.scalableSolutions.restTest.api.order.create;

import com.scalableSolutions.restTest.dataProviders.DataProviderToCreateOrder;
import com.scalableSolutions.restTest.preconditions.ConditionsCreateOrder;
import org.testng.annotations.Test;
import top.jfunc.json.impl.JSONObject;

import static com.scalableSolutions.restTest.ErrorMessages.*;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.equalTo;

public class TestCreateOrder extends ConditionsCreateOrder {

    /**
     * Позитивные тесты по созданию ордера для POST /api/order/create
     */

    /**
     * Задаем только корректные обязательные значения для вводимых полей:
     * @price - цена, по которой пользователь готов покупать или продавать товар.
     * @quantity - количество товара для продажи или покупки
     * @side - тип заявки
     * Ожидаемый результат - код - 201, ордер создан успешно
     */
    @Test(dataProvider = "positiveTestsWithRequiredParameters", alwaysRun = true, dataProviderClass = DataProviderToCreateOrder.class)
    public void positiveTestsWithRequiredParameters(Object price, Object quantity, Object side){
        JSONObject body = new JSONObject();
        body.put("price", price);
        body.put("quantity", quantity);
        body.put("side", side);

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(201)
                .assertThat().body(matchesJsonSchemaInClasspath("jsons/PostApiOrderCreate.json"))
                .body("quantity",equalTo(quantity))
                .body("side",equalTo(side))
                .extract()
                .response();
    }

    /**
     * Задаем все значения для вводимых полей корректно:
     * @id - Идентификатор для ордера. Ордер - это заявка на бирже на покупку или продажу некоторого количества
     * товара (валюты, акции, облигации или другого товара)
     * @price - цена, по которой пользователь готов покупать или продавать товар.
     * @quantity - количество товара для продажи или покупки
     * @side - тип заявки - Buy
     * Ожидаемый результат - код - 201, ордер создан успешно
     */
    @Test(dataProvider = "positiveTestsWithAllParametersWhereSideIsBuyValue", alwaysRun = true, dataProviderClass = DataProviderToCreateOrder.class)
    public void positiveTestsWithAllParametersWhereSideIsBuyValue(Object id, Object price, Object quantity, Object side){
        JSONObject body = new JSONObject();
        body.put("id", id);
        body.put("price", price);
        body.put("quantity", quantity);
        body.put("side", side);

        given().headers("Content-Type", "application/json")
               .body(body.toString())
        .when().post("/api/order/create")
        .then().statusCode(201)
               .assertThat().body(matchesJsonSchemaInClasspath("jsons/PostApiOrderCreate.json"))
               .body("id", equalTo(id))
               .body("price",equalTo(price))
               .body("quantity",equalTo(quantity))
               .body("side",equalTo("buy"))
               .extract()
               .response();
    }

    /**
     * Задаем все значения для вводимых полей корректно:
     * @id - Идентификатор для ордера. Ордер - это заявка на бирже на покупку или продажу некоторого количества
     * товара (валюты, акции, облигации или другого товара)
     * @price - цена, по которой пользователь готов покупать или продавать товар.
     * @quantity - количество товара для продажи или покупки
     * @side - тип заявки - Sell
     * Ожидаемый результат - код - 201, ордер создан успешно
     */
    @Test(dataProvider = "positiveTestsWithAllParametersWhereSideIsSellValue", alwaysRun = true, dataProviderClass = DataProviderToCreateOrder.class)
    public void positiveTestsWithAllParametersWhereSideIsSellValue(Object id, Object price, Object quantity, Object side){
        JSONObject body = new JSONObject();
        body.put("id", id);
        body.put("price", price);
        body.put("quantity", quantity);
        body.put("side", side);

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(201)
                .assertThat().body(matchesJsonSchemaInClasspath("jsons/PostApiOrderCreate.json"))
                .body("id", equalTo(id))
                .body("price",equalTo(price))
                .body("quantity",equalTo(quantity))
                .body("side",equalTo("sell"))
                .extract()
                .response();
    }

    /**
     * Негативные тесты для POST /api/order/create
     */

    /**
     * Не передаем обязательные поля - price, quantity and side
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "You need to pass 'price', 'quantity' and 'side' params"
     */
    @Test()
    public void negativeTestsWithoutMandatoryFields() {
        JSONObject body = new JSONObject();
        body.put("id", "1001");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(YOU_NEED_TO_PASS_PRICE_QUANTITY_AND_SIDE_PARAMS));
    }

    /**
     * Проверка на ввод Несуществующего дополнительного параметра в запросе
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "Param 'test' is not exist"
     */
    @Test()
    public void negativeTestsSettingNotExistAdditionalParameters() {
        JSONObject body = new JSONObject();
        body.put("id", "1005");
        body.put("test", "Test");
        body.put("price", "100");
        body.put("quantity", "222");
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(PARAM_TEST_IS_NOT_EXIST));
    }

    /**
     * Не передаем обязательный параметр - price
     * @quantity - количество товара для продажи или покупки
     * @side - тип заявки
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "You need to pass 'price' param"
     */
    @Test(dataProvider = "negativeTestsWithOutPriceParameter", alwaysRun = true, dataProviderClass = DataProviderToCreateOrder.class)
    public void negativeTestsWithOutPriceParameter(Object quantity, Object side) {
        JSONObject body = new JSONObject();
        body.put("quantity", quantity);
        body.put("side", side);

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(YOU_NEED_TO_PASS_PRICE_PARAM));
    }

    /**
     * Не передаем обязательный параметр - price. Но передаем Id.
     * @id - Идентификатор для ордера. Ордер - это заявка на бирже на покупку или продажу некоторого количества
     * товара (валюты, акции, облигации или другого товара)
     * @quantity - количество товара для продажи или покупки
     * @side - тип заявки
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "You need to pass 'price' param"
     */
    @Test(dataProvider = "negativeTestsWithOutPriceParameterButHaveID", alwaysRun = true, dataProviderClass = DataProviderToCreateOrder.class)
    public void negativeTestsWithOutPriceParameterButHaveID(Object id, Object quantity, Object side) {
        JSONObject body = new JSONObject();
        body.put("id", id);
        body.put("quantity", quantity);
        body.put("side", side);

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(YOU_NEED_TO_PASS_PRICE_PARAM));
    }

    /**
     * Передаем неверное значение для side = buy, где все буквы НЕ заглавные
     * @id - Идентификатор для ордера. Ордер - это заявка на бирже на покупку или продажу некоторого количества
     * товара (валюты, акции, облигации или другого товара)
     * @price - цена, по которой пользователь готов покупать или продавать товар.
     * @quantity - количество товара для продажи или покупки
     * @side - тип заявки
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "side: IncorrectValue"
     */
    @Test(dataProvider = "negativeTestsWhereSideIsbuyValue", alwaysRun = true, dataProviderClass = DataProviderToCreateOrder.class)
    public void negativeTestsWhereSideIsbuyValue(Object id, Object price, Object quantity, Object side) {
        JSONObject body = new JSONObject();
        body.put("id", id);
        body.put("price", price);
        body.put("quantity", quantity);
        body.put("side", side);

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(SIDE_INCORRECT_VALUE));
    }

    /**
     * Передаем неверное значение для side = sell, где все буквы НЕ заглавные
     * @id - Идентификатор для ордера. Ордер - это заявка на бирже на покупку или продажу некоторого количества
     * товара (валюты, акции, облигации или другого товара)
     * @price - цена, по которой пользователь готов покупать или продавать товар.
     * @quantity - количество товара для продажи или покупки
     * @side - тип заявки
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "side: IncorrectValue"
     */
    @Test(dataProvider = "negativeTestsWhereSideIssellValue", alwaysRun = true, dataProviderClass = DataProviderToCreateOrder.class)
    public void negativeTestsWhereSideIssellValue(Object id, Object price, Object quantity, Object side) {
        JSONObject body = new JSONObject();
        body.put("id", id);
        body.put("price", price);
        body.put("quantity", quantity);
        body.put("side", side);

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(SIDE_INCORRECT_VALUE));
    }

    /**
     * Передаем неверное значение для side = BUY, где все буквы НЕ заглавные
     * @id - Идентификатор для ордера. Ордер - это заявка на бирже на покупку или продажу некоторого количества
     * товара (валюты, акции, облигации или другого товара)
     * @price - цена, по которой пользователь готов покупать или продавать товар.
     * @quantity - количество товара для продажи или покупки
     * @side - тип заявки
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "side: IncorrectValue"
     */
    @Test(dataProvider = "negativeTestsWhereSideIsBUYValue", alwaysRun = true, dataProviderClass = DataProviderToCreateOrder.class)
    public void negativeTestsWhereSideIsBUYValue(Object id, Object price, Object quantity, Object side) {
        JSONObject body = new JSONObject();
        body.put("id", id);
        body.put("price", price);
        body.put("quantity", quantity);
        body.put("side", side);

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(SIDE_INCORRECT_VALUE));
    }

    /**
     * Передаем неверное значение для side = SELL, где все буквы НЕ заглавные
     * @id - Идентификатор для ордера. Ордер - это заявка на бирже на покупку или продажу некоторого количества
     * товара (валюты, акции, облигации или другого товара)
     * @price - цена, по которой пользователь готов покупать или продавать товар.
     * @quantity - количество товара для продажи или покупки
     * @side - тип заявки
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "side: IncorrectValue"
     */
    @Test(dataProvider = "negativeTestsWhereSideIsSELLValue", alwaysRun = true, dataProviderClass = DataProviderToCreateOrder.class)
    public void negativeTestsWhereSideIsSELLValue(Object id, Object price, Object quantity, Object side) {
        JSONObject body = new JSONObject();
        body.put("id", id);
        body.put("price", price);
        body.put("quantity", quantity);
        body.put("side", side);

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(SIDE_INCORRECT_VALUE));
    }

    /**
     * Проверка на присвоение пустого значения для id
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID should be an Integer"
     */
    @Test
    public void negativeTestWithoutId() {
        JSONObject body = new JSONObject();
        body.put("id", "");
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим id = 0.
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID can't be less or equal than 0"
     */
    @Test
    public void negativeTestWithIdEqualsZero() {
        JSONObject body = new JSONObject();
        body.put("id", "0");
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_CANT_BE_LESS_OR_EQUAL_THAN_0));
    }

    /**
     * Тест на обработку случая, когда вводим id = -1.
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID can't be less or equal than 0"
     */
    @Test
    public void negativeTestWithNegativeId() {
        JSONObject body = new JSONObject();
        body.put("id", "-1");
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_CANT_BE_LESS_OR_EQUAL_THAN_0));
    }

    /**
     * Тест на обработку случая, когда вводим id = 10000.
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID can't be less or equal than 0"
     */
    @Test
    public void negativeTestExceedingTheMaximumNumber() {
        JSONObject body = new JSONObject();
        body.put("id", "10000");
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_CANT_BE_MORE_OR_EQUAL_THAN_10000));
    }

    /**
     * Тест на обработку случая, когда вводим значение в виде строки, где все символы записаны с НЕ заглавной буквы.
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID should be an Integer"
     */
    @Test
    public void negativeTestWithLowercaseStringId() {
        JSONObject body = new JSONObject();
        body.put("id", "string");
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение в виде строки, где все символы записаны с заглавной буквы.
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID should be an Integer"
     */
    @Test
    public void negativeTestWithUppercaseStringId() {
        JSONObject body = new JSONObject();
        body.put("id", "STRING");
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение в виде строки, где символы с заглавной и с НЕ заглавных букв.
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID should be an Integer"
     */
    @Test
    public void negativeTestWithUpperAndLowercaseStringId() {
        JSONObject body = new JSONObject();
        body.put("id", "STRING");
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение в виде строки, где используем символ нижнего подчеркивания(спецсимвол в строке).
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID should be an Integer"
     */
    @Test
    public void negativeTestWithAnUnderscore() {
        JSONObject body = new JSONObject();
        body.put("id", "string_string");
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение в виде строки, где используем символ тире(спецсимвол в строке).
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID should be an Integer"
     */
    @Test
    public void negativeTestWithDash() {
        JSONObject body = new JSONObject();
        body.put("id", "string-string");
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение в виде числа и строки.
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID should be an Integer"
     */
    @Test
    public void negativeTestWithNumberAndString() {
        JSONObject body = new JSONObject();
        body.put("id", "123String");
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение только в виде спецсимвола.
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID should be an Integer"
     */
    @Test
    public void negativeTestWithWildcard() {
        JSONObject body = new JSONObject();
        body.put("id", "@");
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение ASCII из типа управляющих символов (Табуляция, Перевод строки и т.д.).
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID should be an Integer"
     */
    @Test
    public void negativeTestWithASCIIControlCharacters() {
        JSONObject body = new JSONObject();
        body.put("id", "0x07");
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение ASCII из типа печатных символов (Пробел, #, %, 0, A и т.д.).
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID should be an Integer"
     */
    @Test
    public void negativeTestWithASCIIPrintedSymbol() {
        JSONObject body = new JSONObject();
        body.put("id", "0x21");
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение ASCII из типа WIN-1251 символов.
     * Расширенные символы ASCII Win-1251 кириллица
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID should be an Integer"
     */
    @Test
    public void negativeTestWithASCIIWIN1251Symbols() {
        JSONObject body = new JSONObject();
        body.put("id", "0x80");
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение с пробелами.
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID should be an Integer"
     */
    @Test
    public void negativeTestWithANumberThatContainsSpaces() {
        JSONObject body = new JSONObject();
        body.put("id", "1 2");
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение только из пробелов.
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID should be an Integer"
     */
    @Test
    public void negativeTestWithSpaces() {
        JSONObject body = new JSONObject();
        body.put("id", "   ");
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение с явным превышением количества символов для значения id.
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID can't be more or equal than 10000"
     */
    @Test
    public void negativeTestWithLargeNumber() {
        JSONObject body = new JSONObject();
        body.put("id", "9999999999999999999999999999999");
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_CANT_BE_MORE_OR_EQUAL_THAN_10000));
    }

    /**
     * Тест на обработку случая, когда вводим id = 0. id имеет тип Integer
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID can't be less or equal than 0"
     */
    @Test
    public void negativeTestWithIdIntegerTypeEqualsZero() {
        JSONObject body = new JSONObject();
        body.put("id", 0);
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_CANT_BE_LESS_OR_EQUAL_THAN_0));
    }

    /**
     * Тест на обработку случая, когда вводим id = -1. id имеет тип Integer
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID can't be less or equal than 0"
     */
    @Test
    public void negativeTestWithIdIntegerTypeNegativeId() {
        JSONObject body = new JSONObject();
        body.put("id", -1);
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_CANT_BE_LESS_OR_EQUAL_THAN_0));
    }

    /**
     * Тест на обработку случая, когда вводим id = 10000. id имеет тип Integer
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID can't be less or equal than 0"
     */
    @Test
    public void negativeTestIdIntegerTypeExceedingTheMaximumNumber() {
        JSONObject body = new JSONObject();
        body.put("id", 10000);
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_CANT_BE_MORE_OR_EQUAL_THAN_10000));
    }

    /**
     * Тест на обработку случая, когда вводим значение ASCII из типа управляющих символов (Табуляция, Перевод строки и т.д.).
     * id имеет тип Integer
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID should be an Integer"
     */
    @Test
    public void negativeTestWithIdIntegerTypeASCIIControlCharacters() {
        JSONObject body = new JSONObject();
        body.put("id", 0x07);
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение ASCII из типа печатных символов (Пробел, #, %, 0, A и т.д.).
     * id имеет тип Integer
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID should be an Integer"
     */
    @Test
    public void negativeTestWithIdIntegerTypeASCIIPrintedSymbol() {
        JSONObject body = new JSONObject();
        body.put("id", 0x21);
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение ASCII из типа WIN-1251 символов.
     * Расширенные символы ASCII Win-1251 кириллица
     * id имеет тип Integer
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID should be an Integer"
     */
    @Test
    public void negativeTestWithIdIntegerTypeASCIIWIN1251Symbols() {
        JSONObject body = new JSONObject();
        body.put("id", 0x80);
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение с явным превышением количества символов для значения id.
     * id имеет тип Integer
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID can't be more or equal than 10000"
     */
    @Test
    public void negativeTestWithIdIntegerTypeLargeNumber() {
        JSONObject body = new JSONObject();
        body.put("id", 999999999);
        body.put("price", "100");
        body.put("quantity", 1000);
        body.put("side", "Buy");

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_CANT_BE_MORE_OR_EQUAL_THAN_10000));
    }

    /**
     * id имеет тип отличный от допустимых String/Integer
     * Ожидаемый результат - код - 400, bad request
     * Сообщение об ошибке - "ID should be an Integer"
     */
    @Test(dataProvider = "negativeTestsSettingIncorrectValuesIdIsNotIntegerOrString", alwaysRun = true, dataProviderClass = DataProviderToCreateOrder.class)
    public void negativeTestWithIncorrectTypes(Object id, Object price, Object quantity, Object side) {
        JSONObject body = new JSONObject();
        body.put("id", id);
        body.put("price", price);
        body.put("quantity", quantity);
        body.put("side", side);

        given().headers("Content-Type", "application/json")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Передаем неверные значения для headers
     * Ожидаемый результат - код - 400, bad request
     */
    @Test()
    public void negativeTestsWithIncorrectHeaders() {
        JSONObject body = new JSONObject();
        body.put("price", "21");
        body.put("quantity", "21");
        body.put("side", "buy");

        given().header("Accept-Charset", "windows-1251")
                .body(body.toString())
                .when().post("/api/order/create")
                .then().statusCode(400)
                .body("message",equalTo(HEADER_IS_INCORRECT ));
    }

}
