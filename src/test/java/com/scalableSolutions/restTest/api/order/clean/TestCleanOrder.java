package com.scalableSolutions.restTest.api.order.clean;

import com.scalableSolutions.restTest.preconditions.ConditionsCleanOrders;
import org.testng.annotations.Test;

import static com.scalableSolutions.restTest.ErrorMessages.HEADER_IS_INCORRECT;
import static com.scalableSolutions.restTest.ErrorMessages.ORDER_NOT_FOUND;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.equalTo;

public class TestCleanOrder extends ConditionsCleanOrders {

    /**
     * Позитивные тесты для GET api/order/clean
     */

    /**
     * Тест для очистки информации о заказах.
     * Ожидаемый результат - код - 200, "Order book is clean"
     */
    @Test
    public void positiveTestWithFirstId() {
        given().headers("Content-Type", "application/json")
                .get("api/order?id=21")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchemaInClasspath("jsons/GetApiOrder.json"))
                .body("id", equalTo("21"));

        given().headers("Content-Type", "application/json")
                .get("api/order/clean")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchemaInClasspath("jsons/GetApiOrderClean.json"))
                .body("message", equalTo("Order book is clean"));

        given().get("api/order?id=21")
                .then()
                .statusCode(404)
                .body("message", equalTo(ORDER_NOT_FOUND));
    }

    /**
     * Негативные тесты
     */

    /**
     * HEADERS
     * Использование неверной кодировка символов
     * Ожидаемый результат - код ошибки - 400, запрос невалиден
     */
    @Test
    public void negativeIncorrectEncoding(){
        given().header("Accept-Charset", "windows-1251")
                .get("api/order/clean")
                .then()
                .statusCode(400)
                .body("message", equalTo(HEADER_IS_INCORRECT));
    }

    /**
     * HEADERS
     * Использование неверного Content-type
     * Ожидаемый результат - код ошибки - 400, запрос невалиден
     */
    @Test
    public void negativeIncorrectContentType(){
        given().headers("Content-Type", "text/html")
                .get("api/order/clean")
                .then()
                .statusCode(400)
                .body("message", equalTo(HEADER_IS_INCORRECT));
    }

}
