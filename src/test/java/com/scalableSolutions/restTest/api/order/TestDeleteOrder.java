package com.scalableSolutions.restTest.api.order;

import com.scalableSolutions.restTest.preconditions.ConditionsDeleteOrder;
import org.testng.annotations.Test;

import static com.scalableSolutions.restTest.ErrorMessages.*;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.equalTo;

public class TestDeleteOrder extends ConditionsDeleteOrder {

    /**
     * Позитивные тесты для DELETE api/order
     */

    /**
     * Тест с удалением информации о заказе по id = 3.
     * Ожидаемый результат - код - 204, значение успешно удалено
     */
    @Test
    public void positiveTestWithFirstId() {
        given().get("api/order?id=3")
                .then()
                .statusCode(200)
                .body("id", equalTo("3"));

        given().delete("api/order?id=3")
                .then()
                .statusCode(204)
                .assertThat().body(matchesJsonSchemaInClasspath("jsons/DeleteApiOrder.json"));

        given().get("api/order?id=3")
                .then()
                .statusCode(404)
                .body("message", equalTo(ORDER_NOT_FOUND));
    }


    /**
     * Негативные тесты для DELETE https://reqres.in/api/order
     */


    /**
     * Тест на обработку случая, когда не вводим параметры.
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void positiveTestWithOutParameters() {
        given().delete("api/order")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда не вводим значение для id.
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithOutId() {
        given().delete("api/order?id=")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим id = 0.
     * Предполагаем, что все ids начинаются с 1
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithIdEqualsZero() {
        given().delete("api/order?id=0")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_CANT_BE_LESS_OR_EQUAL_THAN_0));
    }

    /**
     * Тест на обработку случая, когда вводим id = -1. Проверка на обработку отрицательных значений.
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithNegativeId() {
        given().delete("api/order?id=-1")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_CANT_BE_LESS_OR_EQUAL_THAN_0));
    }

    /**
     * Тест на обработку случая, когда вводим id = 10000. Превышение максимально допустимого значения id.
     * Число 10000 = maxValue + 1
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestExceedingTheMaximumNumber() {
        given().delete("api/order?id=10000")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_CANT_BE_MORE_OR_EQUAL_THAN_10000));
    }

    /**
     * Тест на обработку случая, когда пытаемся удалить ордер, которого нет на сервере.
     * Ожидаемый результат - код ошибки - 404, значение не найдено
     */
    @Test
    public void positiveTestWithOrderIsNotExist() {
        given().get("api/order?id=2111")
                .then()
                .statusCode(404)
                .body("message", equalTo(ORDER_NOT_FOUND));
    }

    /**
     * Тест на обработку случая, когда вводим значение в виде строки, где все символы записаны с НЕ заглавной буквы.
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithLowercaseStringId() {
        given().delete("api/order?id=string")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение в виде строки, где все символы записаны с заглавной буквы.
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithUppercaseStringId() {
        given().delete("api/order?id=STRING")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение в виде строки, где символы с заглавной и с НЕ заглавных букв.
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithUpperAndLowercaseStringId() {
        given().delete("api/order?id=String")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение в виде строки, где используем символ нижнего подчеркивания(спецсимвол в строке).
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithAnUnderscore() {
        given().delete("api/order?id=string_string")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение в виде строки, где используем символ тире(спецсимвол в строке).
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithDash() {
        given().delete("api/order?id=string-string")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение в виде числа и строки.
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithNumberAndString() {
        given().delete("api/order?id=123String")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение в виде только в виде спецсимвола.
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithWildcard() {
        given().delete("api/order?id=@")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение ASCII из типа управляющих символов (Табуляция, Перевод строки и т.д.).
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithASCIIControlCharacters() {
        given().delete("api/order?id=0x07")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение ASCII из типа печатных символов (Пробел, #, %, 0, A и т.д.).
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithASCIIPrintedSymbols() {
        given().delete("api/order?id=0x21")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение ASCII из типа WIN-1251 символов.
     * Расширенные символы ASCII Win-1251 кириллица
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithASCIIWIN1251Symbols() {
        given().delete("api/order?id=0x80")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение с пробелами.
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithANumberThatContainsSpaces() {
        given().delete("api/order?id=1 2")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение только из пробелов.
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithSpaces() {
        given().delete("api/order?id=   ")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Тест на обработку случая, когда вводим значение с явным превышением количества символов для значения id.
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithLargeNumber() {
        given().delete("api/order?id=9999999999999999999999999999999")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * HEADERS
     * Использование неверной кодировка символов
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeIncorrectEncoding(){
        given().header("Accept-Charset", "windows-1251")
                .delete("api/order?id=1")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * HEADERS
     * Использование неверного Content-type
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeIncorrectContentType(){
        given().headers("Content-Type", "text/html")
                .delete("api/order?id=1")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Проверка на ввод дополнительных полей в запросе
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithAdditionalParams(){
        given().delete("api/order?id=1&price=100")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id=1&price=100")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id=1&quantity=100")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id=1&side=buy")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id=1&price=100&quantity=100")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id=1&price=100&quantity=100&side=buy")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * Проверка на ввод Несуществующего дополнительного параметра в запросе
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestNotExistAdditionalParam() {
        given().delete("api/order?id=1&test=100")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }



    /**
     * Негативные тесты безопасности для DELETE https://reqres.in/api/order
     */

    /**
     * SQL Injection
     * Обнаружение уязвимости
     * SQL запрос примет DELETE запрос к - SELECT id, name from table where id =1 ORDER BY 100
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithFirstSQLInjection() {
        given().delete("api/order?id=1+ORDER+BY+100")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }


    /**
     * SQL Injection
     * Слепое внедрение операторов SQL
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithBlindSQLInjection() {
        given().delete("api/order?id=-1+OR+1=1")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id=-1+OR+1=1--")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id=-1'")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id=-1'+AND+1=2")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id=-1'+OR+'1'='1")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id=-1\"/*")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id=1'+AND+1=1")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id=1'+AND+'1'='1")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Способы обнаружения Double blind SQL-инъекций
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithDoubleBlindSQLInjection() {
        given().delete("api/order?id=-1+AND+benchmark(2000,md5(now()))")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id=-1'+AND+benchmark(2000,md5(now()))--")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Способы обнаружения MD5-хеш SQL-инъекций, если символ входит в диапазон [0-9a]
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithMD509aSQLInjection() {
        given().delete("api/order?id=1+AND+1+rlike+concat(if((mid((select+pass+from+users+limit+0,1),1,1)in('0'))>0,(0x787B312C323\n" +
                "5367D),if((mid((select+pass+from+users+limit+0,1),1,1)in('1'))>0,(0x787B312C28),if((mid((select+pass+f\n" +
                "rom+users+limit+0,1),1,1)in('2'))>0,(0x5B5B3A5D5D),if((mid((select+pass+from+users+limit+0,1),1,1)in('\n" +
                "3'))>0,(0x5B5B),if((mid((select+pass+from+users+limit+0,1),1,1)in('4'))>0,(0x28287B317D),if((mid((sele\n" +
                "ct+pass+from+users+limit+0,1),1,1)in('5'))>0,(0x0),if((mid((select+pass+from+users+limit+0,1),1,1)in('6'\n" +
                "))>0,(0x28),if((mid((select+pass+from+users+limit+0,1),1,1)in('7'))>0,(0x5B322D315D),if((mid((select+p\n" +
                "ass+from+users+limit+0,1),1,1)in('8'))>0,(0x5B5B2E63682E5D5D),if((mid((select+pass+from+users+limit\n" +
                "+0,1),1,1)in('9'))>0,(0x5C),if((mid((select+pass+from+users+limit+0,1),1,1)in('a'))>0,(select 1 union\n" +
                "select 2),(1)))))))))))))")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Способы обнаружения MD5-хеш SQL-инъекций, если символ не входит в диапазон [0-9a], тогда отправляем запрос проверка [b-f]
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithMD5bfSQLInjection() {
        given().delete("api/order?id=1+AND+1+rlike+concat(if((mid((select+pass+from+users+limit+0,1),1,1)in('0'))>0,(0x787B312C323\n" +
                "5367D),if((mid((select+pass+from+users+limit+0,1),1,1)in('1'))>0,(0x787B312C28),if((mid((select+pass+f\n" +
                "rom+users+limit+0,1),1,1)in('2'))>0,(0x5B5B3A5D5D),if((mid((select+pass+from+users+limit+0,1),1,1)in('\n" +
                "3'))>0,(0x5B5B),if((mid((select+pass+from+users+limit+0,1),1,1)in('4'))>0,(0x28287B317D),if((mid((sele\n" +
                "ct+pass+from+users+limit+0,1),1,1)in('5'))>0,(0x0),if((mid((select+pass+from+users+limit+0,1),1,1)in('6'\n" +
                "))>0,(0x28),if((mid((select+pass+from+users+limit+0,1),1,1)in('7'))>0,(0x5B322D315D),if((mid((select+p\n" +
                "ass+from+users+limit+0,1),1,1)in('8'))>0,(0x5B5B2E63682E5D5D),if((mid((select+pass+from+users+limit\n" +
                "+0,1),1,1)in('9'))>0,(0x5C),if((mid((select+pass+from+users+limit+0,1),1,1)in('a'))>0,(select 1 union\n" +
                "select 2),(1)))))))))))))")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * SQL-инъекции, Работа с файловой системой и выполнение команд на сервере при эксплуатации уязвимости SQL Injection
     * запись web-shell в файл /www/img/shell.php
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithWebShellSQLInjection() {
        given().delete("api/order?id=1+union+select+'<?eval($_request[shell]);?>'+into+outfile+'/www/img/shell.php'")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * SQL-инъекции, Работа с файловой системой и выполнение команд на сервере при эксплуатации уязвимости SQL Injection
     * Пример по обходу сигнатур (обфускация запроса). То есть можем заменять ключевые слова union, select, from
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithRequestObfuscationSQLInjection() {
        given().delete("api/order?id=1+union+(select+1,2+from+test.users)")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id=1+union+(select+'xz'from+xxx)")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id=(1)unIon(selEct(1),mid(hash,1,32)from(test.users))")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id=1+union+(sELect'1',concat(login,hash)from+test.users)")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id=(1)union(((((((select(1),hex(hash)from(test.users))))))))")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id=(1);exec('sel'+'ect'(1))")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id=(1)or(0x50=0x50)")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Использовать null-byte для обхода бинарно-зависимых функций. Пример: if(ereg ("^(.){1,3}$", $_GET['param'])) { … }
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithNullByteSQLInjection() {
        given().delete("api/order?id=1+ereg(\"^(.){1,3}$\",\"123\")")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id=1+union+select+1+ereg(\"^(.){1,3}$\",\"1 union select 1\")")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id=1/*%00*/union+select+1+ereg(\"^(.){1,3}$\",\"1/*\\0*/union select 1\")")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Обход функции addslashes(). Это возможно, если существует уязвимость, позволяющая установить кодировку SJIS, BIG5 или GBK
     * addslashes("'") т.е. 0x27 вернет "\'" т.е. 0x5c27
     * после обработки функцией addslashes() 0xbf27 превращается в 0xbf5c27 т.е. 0xbf5c и одинарную кавычку 0x27
     * Более детальное описание - https://raz0r.name/vulnerabilities/sql-inekcii-svyazannye-s-multibajtovymi-kodirovkami-i-addslashes/
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithAddSlashesSQLInjection() {
        given().delete("api/order?id=1+0x27")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Пример распространенной уязвимости в функциях фильтров безопасности
     * Вместо конструкции (#####, %00, etc) может использоваться любые наборы символов, вырезаемые фильтром
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithSpecialSymbolsSQLInjection() {
        given().delete("api/order?id=1+un/**/ion+sel/**/ect+1,2,3--")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id=1+un/#####/ion+sel/#####/ect+1,2,3--")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id=1+un/%00/ion+sel/%00/ect+1,2,3--")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Использование HTTP Parameter Pollution (HPP)
     * SQL="delete from table where id="+Request.QueryString("id")
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithHPPSQLInjection() {
        given().delete("api/order?id=1/**/union/*&id=*/select/*&id=*/pwd/*&id=*/from/*&id=*/users")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Использование логических запросов OR
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithORSQLInjection() {
        given().delete("api/order?id=1+OR+0x50=0x50")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Использование логических запросов AND
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestWithANDSQLInjection() {
        given().delete("api/order?id=1+and+ascii(lower(mid((select+pwd+from+users+limit+1,1),1,1)))=74")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Вместо знака равенства может использоваться отрицание или неравенство (!=, <>, <, >)
     * Парадокс! Но многие WAF это пропускают. WAF - Web Application Firewall
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestBySubstitutingTheEqualsSQLInjection() {
        given().delete("api/order?id!=1")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id<>1")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id<1")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));

        given().delete("api/order?id>1")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }

    /**
     * SQL Injection
     * Использование шифрования с разным ключом
     * Ожидаемый результат - код ошибки - 400, bad request
     */
    @Test
    public void negativeTestKeySQLInjection() {
        given().delete("api/order?id=╧i╘═╗Г▐╗щ~)°°Р=")
                .then()
                .statusCode(400)
                .body("message",equalTo(ID_SHOULD_BE_AN_INTEGER));
    }
}
