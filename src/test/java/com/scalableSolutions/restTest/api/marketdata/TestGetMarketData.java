package com.scalableSolutions.restTest.api.marketdata;

import com.scalableSolutions.restTest.preconditions.ConditionsMarketData;
import org.testng.annotations.Test;

import static com.scalableSolutions.restTest.ErrorMessages.HEADER_IS_INCORRECT;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.equalTo;

public class TestGetMarketData extends ConditionsMarketData {

    /**
     * Позитивные тесты для GET api/marketdata
     */

    /**
     * Тест с получением информации о снимках.
     * Ожидаемый результат - код - 200, значения найдены
     */
    @Test
    public void positiveTestWithFirstId() {
        given().headers("Content-Type", "application/json")
                .get("api/marketdata")
                .then()
                .statusCode(200)
                .assertThat().body(matchesJsonSchemaInClasspath("jsons/GetApiMarketData.json"));
    }

    /**
     * Негативные тесты
     */

    /**
     * HEADERS
     * Использование неверной кодировка символов
     * Ожидаемый результат - код ошибки - 400, запрос невалиден
     */
    @Test
    public void negativeIncorrectEncoding(){
        given().header("Accept-Charset", "windows-1251")
                .get("api/marketdata")
                .then()
                .statusCode(400)
                .body("message", equalTo(HEADER_IS_INCORRECT));
    }

    /**
     * HEADERS
     * Использование неверного Content-type
     * Ожидаемый результат - код ошибки - 400, запрос невалиден
     */
    @Test
    public void negativeIncorrectContentType(){
        given().headers("Content-Type", "text/html")
                .get("api/marketdata")
                .then()
                .statusCode(400)
                .body("message", equalTo(HEADER_IS_INCORRECT));
    }
}
