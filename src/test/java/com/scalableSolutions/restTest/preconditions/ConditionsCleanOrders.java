package com.scalableSolutions.restTest.preconditions;

import org.testng.annotations.BeforeMethod;

import static com.scalableSolutions.restTest.orders.Order.createOrder;

public class ConditionsCleanOrders extends Conditions {

    /**
     * Задаем предусловия для проверки удаления всех ордеров:
     * createOrder() - создаем список ордеров, которые впоследствии необходимо удалить
     */
    @BeforeMethod
    public void preconditions() {
        createOrder("21", "100", "100", "Buy");
        createOrder("22", "100", "100", "Buy");
        createOrder("23", "100", "100", "Buy");
    }
}
