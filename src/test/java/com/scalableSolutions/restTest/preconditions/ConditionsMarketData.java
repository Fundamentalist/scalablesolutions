package com.scalableSolutions.restTest.preconditions;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import static com.scalableSolutions.restTest.orders.Order.cleanOrders;
import static com.scalableSolutions.restTest.orders.Order.createOrder;

public class ConditionsMarketData extends Conditions {

    /**
     * Задаем предусловия для проверки получения всех ордеров:
     * cleanOrders() - удаляем всю информацию о ранее созданных ордерах
     * createOrder() - создаем список ордеров, у которых можно взять информацию через GET запрос
     */
    @BeforeMethod
    public void preconditions() {
        cleanOrders();
        createOrder("1", "100", "100", "Buy");
        createOrder("5555", "100", "100", "Buy");
        createOrder("9999", "100", "100", "Buy");
    }

    /**
     * Задаем постусловия:
     * cleanOrders() - удаляем всю информацию о ранее созданных ордерах
     */
    @AfterMethod
    public void postConditions() {
        cleanOrders();
    }
}
