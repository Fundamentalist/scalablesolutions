package com.scalableSolutions.restTest.preconditions;

import com.scalableSolutions.restTest.ConfProperties;
import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;

public class Conditions {

    /**
     * Задаем основной URL для всех тестов
     */
    @BeforeClass
    public void setup() {
        RestAssured.baseURI = ConfProperties.getProperty("baseURI");
    }
}
