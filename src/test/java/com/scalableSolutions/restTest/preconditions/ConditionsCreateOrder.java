package com.scalableSolutions.restTest.preconditions;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import static com.scalableSolutions.restTest.orders.Order.cleanOrders;

public class ConditionsCreateOrder extends Conditions {

    /**
     * Задаем предусловия для проверки создания ордеров:
     * cleanOrders() - удаляем всю информацию о ранее созданных ордерах
     */
    @BeforeMethod
    public void preconditions() {
        cleanOrders();
    }

    /**
     * Задаем постусловия:
     * cleanOrders() - удаляем всю информацию о ранее созданных ордерах
     */
    @AfterMethod
    public void postConditions() {
        cleanOrders();
    }
}
