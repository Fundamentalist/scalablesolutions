package com.scalableSolutions.restTest.preconditions;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import static com.scalableSolutions.restTest.orders.Order.cleanOrders;
import static com.scalableSolutions.restTest.orders.Order.createOrder;

public class ConditionsDeleteOrder extends Conditions {

    /**
     * Задаем предусловия для проверки удаления ордера по id:
     * cleanOrders() - удаляем всю информацию о ранее созданных ордерах
     * createOrder() - создаем ордер, который можно впоследствии удалить
     */
    @BeforeMethod
    public void preconditions() {
        cleanOrders();
        createOrder("3", "100", "100", "Buy");
    }

    /**
     * Задаем постусловия:
     * cleanOrders() - удаляем всю информацию о ранее созданных ордерах
     */
    @AfterMethod
    public void postConditions() {
        cleanOrders();
    }
}
